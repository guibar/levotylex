# Levotylex

## Installation

On a new ubuntu machine, run

```
sudo make install-ubuntu
sudo make install
```

To install required apt-packages and pip packages

You must also install MySQL and configure it.

## Development

Before commiting, run `make lint` to get the code linted.
