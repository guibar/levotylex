from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from usermod.models import User as MyUser
from usermod.forms import UsermodChangeForm, UsermodCreationForm
from main.models import Dossier


class PaymentFilter(admin.SimpleListFilter):
    title = _('Paiement')
    parameter_name = 'payment'

    def lookups(self, request, model_admin):
        return (
            ('enligne', _('En Ligne')),
            ('cheque', _('Cheque')),
            ('virement', _('Virement')),
            ('pas_paye', _('Non payé')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'enligne':
            return queryset.filter(payment_received=True)
        if self.value() == 'cheque':
            return queryset.filter(payment_par_cheque=True)
        if self.value() == 'virement':
            return queryset.filter(payment_par_virement=True)
        if self.value() == 'pas_paye':
            return queryset.filter(payment_par_virement=False, payment_par_cheque=False, payment_received=False)


class DossierFilter(admin.SimpleListFilter):
    title = _('Dossier Reçu')
    parameter_name = 'dossier'

    def lookups(self, request, model_admin):
        return (
            ('recu', _('Reçu')),
            ('en_attente', _('En attente')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'recu':
            return queryset.filter(dossier__date_reception__isnull=False)
        if self.value() == 'en_attente':
            return queryset.filter(dossier__isnull=False, dossier__date_reception__isnull=True)


class DossierInline(admin.TabularInline):
    model = Dossier
    fk_name = 'owner'
    show_change_link = True
    fields = ['date_reception', 'pi_recu', 'doleances_recu', 'arret_travail', 'frais_medicaux',
              'cm_recu', 'ord_anc_formule', 'ord_alt', 'hist_tsh', 'divers_recu',
              'link_to_zendesk', 'date_ack_sent']
    readonly_fields = ['link_to_zendesk', 'date_ack_sent']
    can_delete = False


class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field

    inlines = (DossierInline,)

    fieldsets = (
        (_('Contact'), {'fields': (
            ('civilite', 'id'),
            ('prenom', 'nom_de_famille'),
            'adresse',
            ('code_postal', 'ville'),
            ('email', 'telephone'))}
         ),
        (_('Etat Civil'), {'fields': (
            ('nationalite', 'profession'),
            ('date_de_naissance', 'lieu_de_naissance'))}
         ),
        (_('Infos Paiement'), {'fields': (
            ('payment_received', 'payment_par_cheque', 'payment_par_virement', 'payment_date', 'payment_refund'),
            ('payment_trans_id', 'num_cheque'),
            ('link_to_facture'))}
         ),
        (_('Infos Admin'), {'classes': ('collapse',), 'fields':
                            ('date_inscription', 'date_envoi_ch', 'date_envoi_pieces', 'last_login',
                             'email_confirmed', 'plone_step', 'plone_nb_files', 'zendesk_id')}
         ),
        (_('Permissions'), {'classes': ('collapse',), 'fields':
                            ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}
         ),
    )

    add_fieldsets = (
        (_('Contact'),
         {'fields': ('civilite', 'email', 'prenom', 'nom_de_famille', 'adresse',
                     'code_postal', 'ville', 'telephone')}
         ),
        (_('Etat Civil'),
         {'fields': ('nationalite', 'profession', 'date_de_naissance', 'lieu_de_naissance')}
         ),
    )

    form = UsermodChangeForm
    add_form = UsermodCreationForm

    list_display = ('id', 'nom_de_famille', 'prenom', 'email', 'date_de_naissance',
                    'lieu_de_naissance', 'has_payed', 'has_dossier')
    list_display_links = ('nom_de_famille', 'prenom', 'email')
    search_fields = ('email', 'prenom', 'nom_de_famille', '=id', )
    ordering = ('-id',)
    readonly_fields = ('id', 'has_payed', 'has_dossier', 'date_envoi_ch',
                       'date_envoi_pieces', 'date_inscription',
                       'payment_trans_id', 'payment_received', 'payment_refund', 'zendesk_id', 'plone_nb_files',
                       'plone_step', 'last_login', 'email_confirmed', 'link_to_facture', 'payment_date')
    list_filter = (PaymentFilter, DossierFilter)

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.has_payed():
            return self.readonly_fields + ('payment_par_cheque', 'payment_par_virement')
        return self.readonly_fields


admin.site.register(MyUser, CustomUserAdmin)
