from django import forms

from usermod.models import User


class UsermodCreationForm(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'


class UsermodChangeForm(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'
