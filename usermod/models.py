import os.path
import logging
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils import timezone
from django.db.utils import IntegrityError
from django.db import transaction
from django.apps import apps
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from main.post_payment import post_payment


logger = logging.getLogger(__name__)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    id = models.AutoField(primary_key=True)
    CIVILITE_CHOICES = (('Ma', 'Madame'), ('Mo', 'Monsieur'))
    civilite = models.CharField(max_length=2, choices=CIVILITE_CHOICES, null=True)
    email = models.EmailField(_("Adresse courriel"), unique=True)
    prenom = models.CharField(_("Prénom"), max_length=30)
    nom_de_famille = models.CharField(_("Nom de Famille"), max_length=60)

    telephone = models.CharField(_("Téléphone"), max_length=32, null=True)
    adresse = models.CharField(_("Adresse"), max_length=80, null=True)
    code_postal = models.CharField(_("Code Postal"), max_length=6, null=True)
    ville = models.CharField(_("Ville"), max_length=30, null=True)

    profession = models.CharField(_("Profession"), max_length=100, null=True)
    nationalite = models.CharField(_("Nationalité"), max_length=30, null=True)
    date_de_naissance = models.CharField(
        _("Date de naissance"), max_length=20, null=True)
    lieu_de_naissance = models.CharField(
        _("Lieu de naissance"), max_length=50, null=True)

    date_envoi_ch = models.DateTimeField(_("Date d'envoi de la CH"), null=True)
    date_envoi_pieces = models.DateTimeField(_("Date d'envoi de la liste des pièces"), null=True)

    date_inscription = models.DateTimeField(_("Date d'inscription"), null=True)
    is_active = models.BooleanField(_("Actif"), default=True)
    is_staff = models.BooleanField(_("Staff"), default=False, null=False)
    email_confirmed = models.BooleanField(_("Email confirmé"), default=False)

    payment_received = models.BooleanField(_("Payé en ligne"), default=False)
    payment_par_cheque = models.BooleanField(_("Payé par chèque"), default=False)
    payment_par_virement = models.BooleanField(_("Payé par virement"), default=False)
    num_cheque = models.CharField(_("Numero chèque"), max_length=15, null=True, blank=True)

    payment_refund = models.BooleanField(_("Remboursé"), default=False)

    payment_trans_id = models.IntegerField(_("Numéro de transaction"), null=True)
    # code renvoyé par PZ type 28c1b1dd1c37441dba5364e7648331d6
    # je garde on ne sait jamais
    payment_trans_uuid = models.CharField(
        _("Numéro payzen de transaction"), max_length=40, null=True)
    payment_date = models.DateTimeField(_("Date de paiement"), null=True, blank=True, default=None)
    plone_trans_status = models.CharField(
        _("champ 'trans_status' de Plone"), max_length=30, null=True, blank=True)
    plone_trans_id = models.IntegerField(_("trans_id sur Plone"), null=True)

    plone_id = models.CharField(_("champ 'id' de Plone"), max_length=50, null=True, blank=True)
    plone_step = models.IntegerField(_("Champ Step de Plone"), null=True)
    plone_nb_files = models.IntegerField(_("Nb pièces sur Plone"), null=True)
    plone_nb_messages = models.IntegerField(_("Nb pièces sur Plone"), null=True)
    plone_is_orphan = models.BooleanField(_("Utilisateur non souscrit"), default=False)

    zendesk_id = models.BigIntegerField(_("Id Zendesk"), null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['civilite', 'prenom', 'nom_de_famille', 'telephone', 'code_postal',
                       'ville', 'adresse', 'profession', 'date_de_naissance', 'lieu_de_naissance']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    # if payment has just been recorded by cheque, set the time and send facture
    def save(self, *args, **kwargs):
        # in case of payment
        if self.payment_date is None and self.has_payed():
            # set the payment date
            self.payment_date = timezone.now()
            super(User, self).save(*args, **kwargs)
            # create a dossier
            Dossier = apps.get_model('main', 'Dossier')
            try:
                with transaction.atomic():
                    Dossier.objects.create(owner=self)
            except IntegrityError:
                logger.warn("User {} had a dossier already. Bizarre!".format(self.id))
            # do the rest, facture, zendesk, ... asynchronously
            post_payment(self)
        # normal save
        else:
            super(User, self).save(*args, **kwargs)

    def __str__(self):
        return self.get_full_name()

    def get_name_and_email(self):
        name_and_email = "{} ({})".format(self.get_full_name(), self.email)
        return name_and_email.strip()

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = "{} {}".format(self.prenom.title(), self.nom_de_famille.upper())
        return full_name.strip()

    def get_civil_name(self):
        '''
        Returns the complete name with civilité
        '''
        if self.civilite is None:
            return "Monsieur ou Madame {}".format(self.get_full_name())
        else:
            return "{} {}".format(self.get_civilite_display(), self.get_full_name())

    def get_paiement(self):
        if self.payment_received:
            return "carte bancaire"
        elif self.payment_par_cheque:
            return "chèque"
        elif self.payment_par_virement:
            return "virement"
        else:
            return "non réglé"

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.prenom

    def has_payed(self):
        return (self.payment_received or self.payment_par_cheque or self.payment_par_virement)\
               and not self.payment_refund

    has_payed.short_description = "A payé"
    has_payed.boolean = True

    def has_seen_ch(self):
        '''
        Returns true if user has payed, false otherwise
        '''
        return self.date_envoi_ch is not None
    has_seen_ch.boolean = True

    def has_dossier(self):
        '''
        Returns true if user has dossier, false otherwise
        '''
        return self.dossier is not None and self.dossier.bien_recu()
    has_dossier.short_description = "Dossier reçu"
    has_dossier.boolean = True

    def link_to_facture(self):
        if self.facture:
            link = reverse("admin:main_facture_change", args=[self.facture.id])
            return mark_safe('<a href="{}">{}</a>'.format(link, self.facture.id))
        else:
            return "-"
    link_to_facture.short_description = "Facture"

    def link_to_facture_abs(self):
        if self.facture:
            link = 'https://levo.mysmartcab.fr' + reverse("admin:main_facture_change", args=[self.facture.id])
            return mark_safe('<a href="{}">{}</a>'.format(link, "Facture de l'utilisateur {}".format(self.facture.id)))
        else:
            return "-"

    def get_ch_filename(self):
        return 'ch_' + str(self.id) + '_' + self.email + '.pdf'

    def get_ch_path(self):
        return os.path.join(settings.CHS_PATH, self.get_ch_filename())

    def get_rel_ch_path(self):
        return os.path.join(settings.REL_CHS_PATH, self.get_ch_filename())

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)
