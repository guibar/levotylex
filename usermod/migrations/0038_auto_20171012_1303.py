# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-12 11:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0037_auto_20171011_0803'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='num_facture',
            field=models.IntegerField(null=True, verbose_name='Numéro de facture'),
        ),
        migrations.AlterField(
            model_name='user',
            name='payment_par_cheque',
            field=models.BooleanField(default=False, verbose_name='Paiement par chèque'),
        ),
    ]
