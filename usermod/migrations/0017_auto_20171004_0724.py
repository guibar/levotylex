# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-04 07:24
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0016_remove_user_numero'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='lieu_dit',
            new_name='ville',
        ),
    ]
