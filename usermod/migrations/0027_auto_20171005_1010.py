# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-05 08:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0026_auto_20171005_1000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='payment_date',
            field=models.DateTimeField(default=None, null=True, verbose_name='Date de paiement'),
        ),
    ]
