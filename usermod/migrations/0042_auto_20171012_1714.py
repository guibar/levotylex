# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-12 15:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0041_auto_20171012_1624'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='adresse',
            field=models.CharField(blank=True, max_length=80, null=True, verbose_name='Adresse'),
        ),
        migrations.AlterField(
            model_name='user',
            name='code_postal',
            field=models.CharField(blank=True, max_length=6, null=True, verbose_name='Code Postal'),
        ),
        migrations.AlterField(
            model_name='user',
            name='date_de_naissance',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Date de naissance'),
        ),
        migrations.AlterField(
            model_name='user',
            name='email_confirmed',
            field=models.BooleanField(default=False, verbose_name='Email confirmé'),
        ),
        migrations.AlterField(
            model_name='user',
            name='lieu_de_naissance',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Lieu de naissance'),
        ),
        migrations.AlterField(
            model_name='user',
            name='nationalite',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Nationalité'),
        ),
        migrations.AlterField(
            model_name='user',
            name='profession',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Profession'),
        ),
        migrations.AlterField(
            model_name='user',
            name='telephone',
            field=models.CharField(blank=True, max_length=32, null=True, verbose_name='Téléphone'),
        ),
        migrations.AlterField(
            model_name='user',
            name='ville',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Ville'),
        ),
    ]
