# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-30 09:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0010_auto_20170930_0935'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='payment_par_cheque',
            field=models.BooleanField(default=False, verbose_name='A payé'),
        ),
    ]
