# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-10 13:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0035_auto_20171006_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='date_envoi_pieces',
            field=models.DateTimeField(null=True, verbose_name="Date d'envoi de la liste des pièces"),
        ),
    ]
