# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-05 08:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0025_auto_20171005_0913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='date_de_naissance',
            field=models.CharField(blank=True, max_length=16, null=True, verbose_name='Date de naissance'),
        ),
    ]
