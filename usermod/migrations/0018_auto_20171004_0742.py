# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-04 07:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermod', '0017_auto_20171004_0724'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='plone_compte',
        ),
        migrations.AddField(
            model_name='user',
            name='facture_pdf',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Ficher PDF de la facture'),
        ),
        migrations.AddField(
            model_name='user',
            name='payment_trans_uuid',
            field=models.CharField(max_length=30, null=True, verbose_name='Numéro payzen de transaction'),
        ),
        migrations.AddField(
            model_name='user',
            name='plone_nb_messages',
            field=models.IntegerField(null=True, verbose_name='Nb pièces sur Plone'),
        ),
        migrations.AddField(
            model_name='user',
            name='plone_trans_status',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name="champ 'trans_status' de Plone"),
        ),
        migrations.AlterField(
            model_name='user',
            name='ch_pdf',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Ficher PDF de la CH'),
        ),
        migrations.AlterField(
            model_name='user',
            name='date_joined',
            field=models.DateTimeField(null=True, verbose_name="Date d'inscription"),
        ),
    ]
