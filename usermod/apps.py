from django.apps import AppConfig


class UsermodConfig(AppConfig):
    name = 'usermod'
    verbose_name = "Clients"
