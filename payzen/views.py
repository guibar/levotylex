#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from main.models import User
from main.post_payment import build_ch_send_ch_cgu
from .PayZenFormToolBox import *

import calendar
import time
import logging


logger = logging.getLogger(__name__)

payzenTB = PayZenFormToolBox(
    settings.PZ_SITE_ID,    # site id
    settings.PZ_CERT_TEST,  # certificate, TEST-version
    settings.PZ_CERT_PROD,  # certificate, PRODUCTION-version
    settings.PZ_MODE,       # TEST-mode toggle
)


@login_required
def pay(request):
    amount = 9600  # payment amount
    # a daily-unique transaction id - Change-it to reflect your needs
    trans_id = str(calendar.timegm(time.gmtime()))[-6:]
    request.user.payment_trans_id = trans_id
    request.user.save()
    form = payzenTB.form(trans_id, amount, VADS_EURO, request.user.email,
                         request.user.prenom, request.user.nom_de_famille)
    if not request.user.date_envoi_ch:
        build_ch_send_ch_cgu(request.user, 'ch_pdf.html')
    return render(request, 'pay.html', {"form": form})


@csrf_exempt
def ipn(request):
    """Call back method for payzen"""

    assert request.method == 'POST'
    data = request.POST
    try:
        # find the user from the pz info
        user = User.objects.get(email=data['vads_cust_email'])
    # Where is our user gone???, we can't log the transaction anywhere
    except ObjectDoesNotExist:
        logger.critical("The transaction {} cannot be connected to a valid user".format(data))
        return HttpResponse('')
    try:
        payzenTB.ipn(data)
        # No exception, the payment is valid and authorised
        # here the code for an accepted payment
        user.payment_received = True
        # we have a user but the transactions id don't match, ???
        # the data is a string need to cast
        if not user.payment_trans_id == int(data['vads_trans_id']):
            msg = "For user {}, we expected transaction {} but got {}".format(
                user.email, user.payment_trans_id, data['vads_trans_id'])
            logger.error(msg)
            # keep the trans_id sent by PZ
            user.payment_trans_id = int(data['vads_trans_id'])
        # keep this just in case
        user.payment_trans_uuid = data['vads_trans_uuid']
        user.save()
        return HttpResponse('')

    except PayZenPaymentRefused:
        # here the code for a refused payment
        logger.info("The following transaction was refused: {}".format(data['vads_trans_id']))
        return HttpResponse('')
    except PayZenPaymentInvalidated:
        # here the code for a invalidated payment
        # ie when a customer cancels it
        logger.info("The following transaction was cancelled by the user: {}".format(
            data['vads_trans_id']))
        return HttpResponse('')
    except PayZenPaymentPending:
        # here the code for a payment not yet validated
        # could be to mark the corresponding order to 'pending' status
        logger.info("The following transaction is set to pending: {}".format(data['vads_trans_id']))
        return HttpResponse('')
