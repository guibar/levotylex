unexport DJANGO_SETTINGS_MODULE

all: db develop coverage

setup-db:
	echo "CREATE DATABASE IF NOT EXISTS levotylex_base" | mysql -udjango_test -pnuag3s
	echo "CREATE DATABASE IF NOT EXISTS levotylex_test" | mysql -udjango_test -pnuag3s

clean:
	find . -name \*.pyc -delete
	find . -name \*__pycache__ -delete

makemigrations:
	python3 manage.py makemigrations

migrate: makemigrations
	python3 manage.py migrate

db: setup-db migrate

install:
	pip3 install -U -r requirements.txt

install-test:
	pip3 install -U -r requirements/test.txt

install-ubuntu:
	apt-get install build-essential libssl-dev libffi-dev python-dev libcurl4-openssl-dev
	apt-get install mysql-client libmysqlclient-dev libjpeg-dev

show_urls:
	python3 manage.py show_urls

serve: clean
	python3 manage.py runserver

test: clean
	python3 manage.py test --settings levotylex.settings.test

test-ci: clean
	python3 manage.py test --settings levotylex.settings.ci

autoflake:
	find . -name '*.py;-manage.py' |xargs autoflake --in-place --remove-all-unused-imports --remove-unused-variables

autopep8:
	autopep8 --in-place --recursive --max-line-length=100 --exclude="*/migrations/*" .

lint: autoflake autopep8
	flake8

check: lint test
