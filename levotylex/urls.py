"""levotylex URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from main import views
from main import admin as adm_main
from payzen import views as pz_views


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', views.profile, name='profile'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^signin/$', views.signin, name='signin'),
    url(r'^cgu/$', views.cgu, name='cgu'),
    url(r'^ch/$', views.ch, name='ch'),
    url(r'^faq/$', views.faq, name='faq'),
    url(r'^counter/$', views.counter, name='counter'),
    url(r'^account_activation_sent/$', views.account_activation_sent, name='account_activation_sent'),
    url(r'^account_activation_sent/(?P<email>\S{1,64}@\S{1,255})/$',
        views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),

    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),

    url(r'^pay/$', pz_views.pay, name='payment.pay'),
    url(r'^pay/ipn$', pz_views.ipn, name='payment.ipn'),

    url(r'^admin/main/facture_pdf/(?P<facture_id>[0-9]+)/$', adm_main.get_facture_admin, name='facture_admin'),
    url(r'^admin/main/piece/(?P<owner_id>[0-9]+)/(?P<pid>[1-6])$', adm_main.get_piece, name='piece'),
    url(r'^admin/main/assignation_list/(?P<assignation_id>[0-9]+)/$',
        adm_main.get_list_plaignants, name='list_plaignants'),
    url(r'^admin/main/assignation_bordereau/(?P<assignation_id>[0-9]+)/$', adm_main.get_bordereau, name='bordereau'),
    url(r'^admin/main/assignation_csv/(?P<assignation_id>[0-9]+)/$', adm_main.plaignants_csv, name='plaignants_csv'),
    url(r'^admin/main/assignation_non_produites/(?P<assignation_id>[0-9]+)/$',
        adm_main.get_pieces_non_produites, name='non_produites'),
    url(r'^admin/main/pieces_manquantes/$', adm_main.show_pieces_manquantes, name='pieces_manquantes'),
    url(r'^admin/main/pieces_manquantes_pas_vus/$',
        adm_main.show_pieces_manquantes_pas_vus, name='pieces_manquantes_pas_vus'),
    url(r'^admin/main/pieces_manquantes_vus/$', adm_main.show_pieces_manquantes_vus, name='pieces_manquantes_vus'),
    url(r'^admin/main/assignations_stats/$', adm_main.resume_manquants, name='resume_manquants'),

    url(r'^facture/$', views.get_facture, name='facture'),
    url(r'^ch_pdf/$', views.get_ch, name='ch_pdf'),
]
