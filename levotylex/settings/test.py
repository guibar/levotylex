"""
Django settings for levotylex project.

Generated by 'django-admin startproject' using Django 1.11.5.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'main/static')

CGU_FILE_PATH = os.path.join(BASE_DIR, 'main/static/pdfs/cgu.pdf')
PIECES_FILE_PATH = os.path.join(BASE_DIR, 'main/static/pdfs/pieces.pdf')
REL_FACTURES_PATH = '/userdocs/factures/'
REL_AVOIRS_PATH = '/userdocs/avoirs/'
REL_CHS_PATH = '/userdocs/conv_honoraires/'
REL_DOSSIERS_PATH = '/userdocs/dossiers/'

FACTURES_PATH = '/var/levo/factures'
AVOIRS_PATH = '/var/levo/avoirs'
CHS_PATH = '/var/levo/conv_honoraires/'
DOSSIERS_PATH = '/var/levo/dossiers/'
DOSSIERS_VALIDES_PATH = '/var/levo/valides/'
ASSIGNATIONS_PATH = '/var/levo/assignations/'
ARCHIVES_PATH = '/var/levo/archives/'

DAV_DOSSIERS_A_VALIDER_PATH = '/var/dav_levo/a_valider/'
DAV_DOSSIERS_VALIDES_PATH = '/var/dav_levo/valides/'
DAV_DOSSIERS_AVOCATS_PATH = '/var/dav_levo/rs/'

TAMPON_PATH = os.path.join(BASE_DIR, 'scripts/wm.fodt')
UNOCONV_PATH = '/home/gui/.local/bin/unoconv'
ASSIGNATION_P1_PATH = os.path.join(BASE_DIR, 'main/templates/assignation_part_1.fodt')
ASSIGNATION_P2_PATH = os.path.join(BASE_DIR, 'main/templates/assignation_part_2.pdf')
ASSIGNATION_P4_PATH = os.path.join(BASE_DIR, 'main/templates/assignation_part_4.pdf')

PZ_URL_IPN = 'http://myleo.me:83/pay/ipn'
PZ_URL_RETURN = 'http://myleo.me:83/profile?pz'
PZ_SITE_ID = '97949840'
PZ_CERT_TEST = '9164680526622686'
PZ_CERT_PROD = '1660491276744420'
PZ_MODE = 'TEST'  # PRODUCTION or TEST

# where to go after login
LOGIN_REDIRECT_URL = '/profile'
# where to go after logout
LOGOUT_REDIRECT_URL = '/signin'
# where to go when trying to access a page that requires login
LOGIN_URL = '/signin'

EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'apikey'
EMAIL_HOST_PASSWORD = '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'
EMAIL_USE_TLS = True

CORS_ORIGIN_WHITELIST = (
    'www.mysmartcab.fr'
)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '7n6d)cu231!@-%46j=nq4%0k0l3-mmzn3ovtltas+6!a5cdh5+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '212.47.229.192', 'myleo.me']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'main',
    'usermod',
    'bootstrap3',
    'corsheaders'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware'
]

ROOT_URLCONF = 'levotylex.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['main/templates', ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'levotylex.context_processors.debug',
            ],
        },
    },
]

WSGI_APPLICATION = 'levotylex.test.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'levotylex_test',
        'USER': 'django_test',
        'PASSWORD': 'nuag3s',
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
]


AUTH_USER_MODEL = 'usermod.User'

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'fr-FR'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
