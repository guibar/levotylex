from mailin import Mailin
from django.utils import timezone
from levotylex.delayed_jobs import postpone
import logging

logger = logging.getLogger(__name__)


@postpone
def send_pieces(user):
    m = Mailin("https://api.sendinblue.com/v2.0", "CZJYrgjP1O5sW7vh")
    data = {"id": 54,
            "to": user.email,
            "headers": {"Content-Type": "text/html;charset=iso-8859-1"},
            }
    result = m.send_transactional_template(data)
    if (result['code'] == 'success'):
        user.date_envoi_pieces = timezone.now()
        user.save()
        logger.info("Les pieces ont été envoyées à l'utilisateur {}".format(user.id))
    else:
        logger.info("Les pieces n'ont pas été envoyées à l'utilisateur {}".format(str(user.id)))
        print("Les pieces n'ont pas été envoyées à l'utilisateur {}".format(str(user.id)))
    return


@postpone
def send_dossier_ack(dossier):
    m = Mailin("https://api.sendinblue.com/v2.0", "CZJYrgjP1O5sW7vh")
    data = {"id": 55,
            "to": dossier.owner.email,
            "headers": {"Content-Type": "text/html;charset=iso-8859-1"},
            }
    result = m.send_transactional_template(data)
    if (result['code'] == 'success'):
        logger.info("Accusé de reception de dossier envoyé à l'utilisateur {}".format(dossier.owner.id))
    else:
        logger.info("Les pieces n'ont pas été envoyées à l'utilisateur {}".format(str(dossier.owner.id)))
        print("Les pieces n'ont pas été envoyées à l'utilisateur {}".format(str(dossier.owner.id)))
    return
