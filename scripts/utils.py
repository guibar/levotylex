from usermod.models import User
from main.models import Dossier
from main.models import Facture
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from easy_pdf import rendering
from main.zendesk import create_or_update_dossier_ticket
import time
import os.path
import re
import os
import glob
import shutil


def rebuild_ch():
    """Method to generate a PDF for CH
       Receives an user object and a template file"""

    set = User.objects.filter(date_envoi_ch__isnull=False)
    print(set.count())
    for user in set:
        if not os.path.isfile(user.get_ch_path()):
            date_envoi = user.date_envoi_ch
            print(user.get_ch_path())
            ch_pdf_vars = {'user': user, 'date_envoi': date_envoi}
            content = rendering.render_to_pdf('ch_pdf.html', ch_pdf_vars)
            with open(user.get_ch_path(), 'wb') as save_file:
                save_file.write(content)


def rename_factures():
    directory = './userdocs/factures/'

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".pdf"):
            print(filename)
            m = re.search('(\d+).*', filename)
            facture_id = m.group(1)
            try:
                fact = Facture.objects.get(pk=facture_id)
                new_name = 'facture_' + str(fact.owner_id) + '_' + str(facture_id) + '.pdf'
#                print(directory + filename)
#                print(directory + new_name)
                os.rename(directory + filename, directory + new_name)
            except ObjectDoesNotExist:
                print('facture ' + str(facture_id) + ' not found')
                exit()
# create folders, with id as the names, between path and pieces


def move_pieces_into_folders(path):
    m = re.compile(r'(?P<dossier_id>\d+)(?P<dot_>_|\.)(?P<extra0s>0*)(?P<piece_id>\d+).pdf$')
    for filepath in glob.glob(path + '/*.pdf'):
        head, tail = os.path.split(filepath)
        dossier_id = m.search(filepath).group('dossier_id')
        dir_to_create = os.path.join(path, dossier_id)
        if not os.path.isdir(dir_to_create):
            os.makedirs(dir_to_create)
        # move pdf into created dir
        os.rename(filepath, os.path.join(dir_to_create, tail))


def get_dossier_id_from_num(num_in_assignation):
    d = Dossier.objects.get(num_in_assignation=num_in_assignation)
    return d


def create_dossiers_from_assignations(path):
    rex = re.compile(r'^A\.(?P<num_ass_id>(\d)+)\.(?P<pid>[1-6])')
    for filepath in glob.glob(path + '/*/*.pdf'):
        head, tail = os.path.split(filepath)
        ma = rex.search(tail)
        num = ma.group('num_ass_id')
        d = get_dossier_id_from_num(num)

        if not os.path.isdir(d.get_dir()):
            os.makedirs(d.get_dir())
        s = re.sub(r'^A\.(?P<num_ass_id>(\d)+)\.(?P<pid>[1-6])',
                   str(d.owner.id) + r".\g<pid>", tail)
        new_filepath = os.path.join(d.get_dir(), s)
        print(new_filepath)
        shutil.copyfile(filepath, new_filepath)


def create_dossiers():
    users = User.objects.filter(Q(payment_received=True) | Q(payment_par_cheque=True))
    for user in users:
        try:
            print(str(user.dossier) + 'exists')
        except Dossier.DoesNotExist:
            Dossier.objects.create(owner=user)
            print(' create a dossier for ' + user.email)


def create_dossier_tickets_for_all_paying_client():
    users = User.objects.filter(
                Q(payment_received=True) | Q(payment_par_cheque=True))
    print(users.count())
    for user in users:
        print(user)
        create_or_update_dossier_ticket(user)
        time.sleep(2)

# reste de code pour deplacer un dossier en rajoutant ce qui est nouveau
