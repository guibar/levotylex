#!/bin/sh

current_dir=$(pwd)

if [ $current_dir != '/var/www/levotylex.test' ] 
then 
   echo "Not in correct dir... Exiting"
   exit 1
fi

echo "Deploying test: $(date)"

# baixar a ultima versão
git pull origin master

# backup da base de dados
sudo automysqlbackup
# TODO: imprimir o nome do arquivo de backup do banco

# roda migration
export DJANGO_SETTINGS_MODULE=levotylex.settings.test
python3 manage.py migrate

# restart server
sudo service uwsgi reload

