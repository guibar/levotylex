import os
import re
from collections import defaultdict

nas_archives = 'Z:/archives/'
nas_a_traiter = 'Z:/dossier a controler pour archive/'


def nas_move_files(folder_path, dry_run=False):
    for ff in os.listdir(folder_path):
        ff_path = os.path.join(folder_path, ff)
        if os.path.isfile(ff_path):
            name_strict_file = re.compile(r'(?P<dossier_id>\d+)_(?P<pid>[1-3])\.pdf$')
            ns = name_strict_file.search(ff)
            if not ns:
                print('{} file has improper name'.format(ff))
            else:
                # d_dir is the folder where the file should go
                d_dir = os.path.join(folder_path, ns.group('dossier_id'))
                # create it if it doesn't exist
                if not os.path.isdir(d_dir):
                    os.mkdir(d_dir)
                # if there is no file with the same name in d_dir, put the file there
                if not os.path.isfile(os.path.join(d_dir, ff)):
                    os.rename(ff_path, os.path.join(d_dir, ff))
                # if there is increment the number after the _
                else:
                    print('Cannot move {} into directory {}'.format(ff, d_dir))


def nas_rename_files(folder_path, dry_run=False):
    for ff in os.listdir(folder_path):
        # ignore directories
        if os.path.isfile(os.path.join(folder_path, ff)):
            name_loose_file = re.compile(r'(?P<dossier_id>\d+)_(?P<extra0s>0*)(?P<pid>[1-2])\.pdf$')
            ns = name_loose_file.search(ff)
            if ns and ns.group('extra0s'):
                new_filename = name_loose_file.sub(r'\g<dossier_id>_\g<pid>.pdf', ff, 1)
                if dry_run:
                    print('Will rename {} into {}'.format(ff, new_filename))
                elif not os.path.isfile(os.path.join(folder_path, new_filename)):
                    os.rename(os.path.join(folder_path, ff), os.path.join(folder_path, new_filename))
                else:
                    print('Cannot rename {} into {}. File exists'.format(ff, new_filename))


def nas_check_dirs(dry_run=False):
    for ff in os.listdir(nas_archives):
        if not os.path.isfile(os.path.join(nas_archives, ff)):
            check_archive_dir(os.path.join(nas_archives, ff))


def check_archive_dir(folder_path, dry_run=False):
    parent, dir_name = os.path.split(folder_path)
    name_strict = re.compile(r'{}_[1-2]\.pdf$'.format(dir_name))
    for filename in os.listdir(folder_path):
        if not os.path.isfile(os.path.join(folder_path, filename)):
            print('There should be no directory in {}'.format(folder_path))
        elif not name_strict.search(filename):
            print(filename + ' is not named properly')


# look for folders that have a integer name and put them
# into a dictionary with the int as the key and the path as the value
def get_dossiers_path_as_dict(folder_path):
    my_dict = {}
    for dir_path, subdirList, fileList in os.walk(folder_path):
        parent, dir_name = os.path.split(dir_path)
        try:
            id = int(dir_name)
            my_dict[id] = dir_path
        except ValueError:
            print(dir_name + " is not an int")
    print(len(my_dict))
    return my_dict


# look for folders that have a integer name and put them
# into a dictionary with the int as the key and the path as the value
def get_dossiers_path_as_list_dict(folder_path):
    my_dict = defaultdict(list)
    for dir_path, subdirList, fileList in os.walk(folder_path):
        parent, dir_name = os.path.split(dir_path)
        try:
            id = int(dir_name)
            # will append to the list or create one if none is there
            my_dict[id].append(dir_path)
        except ValueError:
            print(dir_name + " is not an int")
    return my_dict


def do_basic_cleaning():
    for dir_path, subdirList, fileList in os.walk(nas_a_traiter):
        parent, dir_name = os.path.split(dir_path)
        try:
            id = int(dir_name)
            for filename in fileList:
                add_file_to_archive_or_not(dir_path, filename, id)
        except ValueError:
            for filename in fileList:
                print("file {} in dir {}".format(filename, dir_path))
#                add_file_to_archive_or_not(dir_path, filename, id)
#            print(dir_name + " is not an int")


def add_file_to_archive_or_not(dir_path, filename, id):
    # don't assume anything about the filename except that the file is in a directory named id
    # check if it has the same size as any file in archive, if it has remove it
    archive_dir = os.path.join(nas_archives, str(id))
    filepath = os.path.join(dir_path, filename)

    if os.path.exists(archive_dir):
        for archive_file in os.listdir(archive_dir):
            archive_file_path = os.path.join(archive_dir, archive_file)
            if os.path.getsize(filepath) == os.path.getsize(archive_file_path):
                print("{} has the same size as {}. Will remove it.".format(filepath, archive_file_path))
                os.remove(filepath)
                return

    # else if it has a x_n name try to keep or increment until ok
    # if it has a d.n name, try to keep it and do d.n.m if it clashes
    # for other cases, just print the filename for the moment

    n1 = re.compile(r'{}_(?P<pid>[1-5])\.pdf$'.format(id))
    n2 = re.compile(r'{}\.(?P<pid>[1-6])\.pdf$'.format(id))

    new_file_path = os.path.join(archive_dir, filename)
    if not os.path.exists(new_file_path):
        if not os.path.exists(archive_dir):
            os.makedirs(archive_dir)
        if n1.search(filename):
            pass
            # print(filepath)
            # os.rename(filepath, new_file_path)
        elif n2.search(filename):
            pass
            # print(filepath)
            # os.rename(filepath, new_file_path)
        else:
            # pass
            print('will rename ' + filepath)
            os.rename(filepath, new_file_path)

    else:
        print(filepath)
        # new_file_path = os.path.join(archive_dir, filepath[:-4]+'.1.pdf')
        # os.rename(filepath, new_file_path)


def rename_qnn():
    name_finder = re.compile(r'.*\((?P<dossier_id>\d+)\)$')
    for dir_path, subdirList, fileList in os.walk(os.path.join(nas_a_traiter, 'Pièces reçues')):
        parent, dir_name = os.path.split(dir_path)
        ns = name_finder.search(dir_name)
        if ns:
            os.rename(dir_path, os.path.join(parent, ns.group('dossier_id')))
#            print(ns.group('dossier_id'))


def print_set(sss):
    for key, list in sss.items():
        print("{},".format(key), end='')
        for el in list:
            print("{},".format(el), end='')
        print('')


def get_diffs():
    h1 = get_dossiers_path_as_dict(nas_archives)
    h2 = get_dossiers_path_as_list_dict(nas_a_traiter)

    s1 = set(h1.keys())
    s2 = set(h2.keys())

    s1m2 = s1 - s2
    s2m1 = s2 - s1
    s1i2 = s1.intersection(s2)

    for key in s1i2:
        print("{},".format(key), end='')
        for el in h1[key]:
            print("{},".format(el), end='')
        for el in h2[key]:
            print("{},".format(el), end='')
        print('')

    for key in s1m2:
        print("{},".format(key), end='')
        for el in h1[key]:
            print("{},".format(el), end='')
        print('')

    for key in s2m1:
        print("{},,".format(key), end='')
        for el in h2[key]:
            print("{},".format(el), end='')
        print('')
