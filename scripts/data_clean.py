# -*- coding: utf-8 -*-
from usermod.models import User
import re
from django.db.models import Q


def find_6d_dates(dry_run=False):
    m = re.compile(r'^(?P<dd>\d\d)(?P<mm>\d\d)(?P<yy>\d\d)$')

    for user in User.objects.all():
        mx = m.search(str(user.date_de_naissance))
        if mx:
            newd = '{}/{}/{}'.format(mx.group('dd'), mx.group('mm'), '19' + mx.group('yy'))
            print(user.date_de_naissance)
            print(newd)
            if not dry_run:
                user.date_de_naissance = newd
                user.save()


def find_6d_slash_dates(dry_run=False):
    m = re.compile(r'^(?P<dd>\d\d)/(?P<mm>\d\d)/(?P<yy>\d\d)$')

    for user in User.objects.all():
        mx = m.search(str(user.date_de_naissance))
        if mx:
            newd = '{}/{}/{}'.format(mx.group('dd'), mx.group('mm'), '19' + mx.group('yy'))
            print(user.date_de_naissance)
            print(newd)
            if not dry_run:
                user.date_de_naissance = newd
                user.save()


def find_8d_dates(dry_run=False):
    m = re.compile(r'^(?P<dd>\d\d)(?P<mm>\d\d)(?P<yyyy>\d\d\d\d)$')

    for user in User.objects.all():
        mx = m.search(str(user.date_de_naissance))
        if mx:
            newd = '{}/{}/{}'.format(mx.group('dd'), mx.group('mm'), mx.group('yyyy'))
            print(user.date_de_naissance)
            print(newd)
            if not dry_run:
                user.date_de_naissance = newd
                user.save()


def print_bad_dates():
    m = re.compile(r'^(?P<dd>\d\d)/(?P<mm>\d\d)/(?P<yyyy>\d\d\d\d)$')
    for user in User.objects.filter(Q(payment_received=True) |
                                    Q(payment_par_cheque=True) | Q(payment_par_virement=True)):
        mx = m.search(str(user.date_de_naissance))
        if not mx:
            print(user.id)


print_bad_dates()
# find_6d_slash_dates()
# find_6d_dates()
# find_8d_dates()
