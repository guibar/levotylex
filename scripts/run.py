from main.models import Assignation

from main.dossier import check_sub_folders
from main.dossier import assign_valides

from main.assignation import set_num_assignation
from main.assignation import create_assignation_folders
from main.assignation import stamp_assignation
from main.assignation import gen_plaignants_csv_file
# from main.assignation import clear_assignation
# from main.assignation import reset_num_assignation


if False:
    check_sub_folders('/var/dav_levo/valides/', True)
    check_sub_folders('/var/dav_levo/valides/')
    # rsync from cloud
    assign_valides()

# fix chown
# set dossiers read only,
# sudo chmod ug-w * -R /var/levo/dossiers


for x in []:
    set_num_assignation(x)
    create_assignation_folders(x)
    stamp_assignation(x)

#    check plaignant data ...

for x in []:
    gen_plaignants_csv_file(x)
    Assignation.objects.get(assignation_id=x).generate_pdf()
