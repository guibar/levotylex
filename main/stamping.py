import os.path
from pdfrw import PdfReader, PdfWriter, PageMerge
from django.conf import settings
from subprocess import call


def stamp_piece(piece_path):
    dir_file = os.path.split(os.path.abspath(piece_path))
    piece_dir = dir_file[0]
    piece_file = dir_file[1]
    # this includes the A.x.x
    piece_name = piece_file[:-4]  # remove .pdf
    tampon_odt_path = os.path.join(piece_dir, "wm." + piece_name + ".fodt")
    tampon_pdf_path = os.path.join(piece_dir, "wm." + piece_name + ".pdf")
    # create tampon odt
    create_tampon_odt(tampon_odt_path, piece_name)
    # convert tampon to pdf
    # windows
    if os.name == 'nt':
        call(["python", settings.UNOCONV_PATH, "-f", "pdf", tampon_odt_path])
    # linux
    else:
        call([settings.UNOCONV_PATH, "-f", "pdf", tampon_odt_path])
    merge_pdfs(piece_path, tampon_pdf_path)
    os.remove(tampon_odt_path)
    os.remove(tampon_pdf_path)


def create_tampon_odt(tampon_odt_path, piece_name):
    with open(settings.TAMPON_PATH, "rt") as fin:
        with open(tampon_odt_path, "wt") as fout:
            for line in fin:
                fout.write(line.replace('##', piece_name))


def merge_pdfs(piece_path, tampon_pdf_path):
    # stamp tampon pdf on piece in the same file
    ipdf = PdfReader(piece_path)
    wpdf = PdfReader(tampon_pdf_path)
    wmark = PageMerge().add(wpdf.pages[0])[0]

    doc_width = float(ipdf.pages[0].MediaBox[2])
    doc_height = float(ipdf.pages[0].MediaBox[3])
    wmark.x = doc_width - wmark.w
    wmark.y = doc_height - wmark.h

    PageMerge(ipdf.pages[0]).add(wmark).render()
    PdfWriter().write(piece_path, ipdf)
