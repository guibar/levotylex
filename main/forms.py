from django import forms
from django.forms import ModelForm, Form
from usermod.models import User


class SignInForm(Form):
    email = forms.EmailField(max_length=50, required=True, help_text='Champ obligatoire')


class ProfileForm(ModelForm):
    email = forms.EmailField(label="Email", max_length=254, disabled=True)
    # voie = forms.CharField(label="Voie", max_length=30)
    # numero = forms.CharField(label="Numéro", max_length=5)
    # comp_add = forms.CharField(label="Complément d'adresse", max_length=30,required=False)
    # localité = forms.CharField(label="Localité", max_length=30)
    # lieu_dit_bp = forms.CharField(label="Lieu dit", max_length=30,required=False)
    # code_postal = forms.CharField(label="Code Postal", max_length=5)
    # phone = forms.CharField(label="Téléphone", max_length=30,required=False)

    # profession = forms.CharField(label="Profession")
    # date_de_naissance = forms.DateField(label="Date de naissance")
    # lieu_de_naissance = forms.CharField(label="Lieu de naissance")

    class Meta:
        model = User
        fields = ['email', 'civilite', 'prenom', 'nom_de_famille', 'adresse', 'code_postal', 'ville',
                  'telephone', 'date_de_naissance', 'lieu_de_naissance', 'nationalite', 'profession']
