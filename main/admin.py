from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Q
from main.models import Dossier, Facture, Assignation
from main.assignation import write_plaignants_csv
from usermod.models import User
import codecs


class DossierPiecesFilter(admin.SimpleListFilter):
    title = _('Pièces')
    parameter_name = 'pieces'

    def lookups(self, request, model_admin):
        return (
            ('ni_cm_ni_ord', _('Ni CM, ni Ord')),
            ('manque_ci', _('Manque P.ID.')),
            ('manque_dol', _('Manque DOL.')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'ni_cm_ni_ord':
            return queryset.filter(Q(date_reception__isnull=False)
                                   & Q(cm_recu=False) & Q(ord_alt=False) & Q(ord_anc_formule=False))
        elif self.value() == 'manque_ci':
            return queryset.filter(Q(date_reception__isnull=False)
                                   & (Q(cm_recu=True) | Q(ord_alt=True) | Q(ord_anc_formule=True))
                                   & Q(pi_recu=False))
        elif self.value() == 'manque_dol':
            return queryset.filter(Q(date_reception__isnull=False)
                                   & (Q(cm_recu=True) | Q(ord_alt=True) | Q(ord_anc_formule=True))
                                   & Q(doleances_recu=False))


class DossierAdmin(admin.ModelAdmin):
    fields = (('link_dossier_to_user', 'link_to_zendesk', 'date_ack_sent'),
              ('state', 'date_reception'),
              ('pi_recu', 'link_to_piece1'),
              ('doleances_recu', 'link_to_piece2'),
              ('cm_recu', 'link_to_piece3'),
              ('ord_anc_formule', 'ord_alt', 'link_to_piece4'),
              ('hist_tsh', 'link_to_piece5'),
              ('divers_recu', 'link_to_piece6'),
              ('arret_travail', 'frais_medicaux'),
              ('assignation', 'num_in_assignation'),
              'commentaires')
    list_display = ('get_dossier_id', 'get_owner_name', '_pi_recu', '_dol_recu', '_cm_recu', '_ord_anc_formule',
                    '_ord_alt', '_hist_tsh', '_divers_recu', 'date_reception', 'link_to_assignation', 'state')
    search_fields = ('owner__email', 'owner__nom_de_famille', 'owner__prenom', '=owner__id')
    list_filter = ('state', DossierPiecesFilter, 'assignation')
    readonly_fields = ['link_dossier_to_user', 'link_to_zendesk', 'date_ack_sent', 'get_owner_name',
                       'get_dossier_id', 'assignation', 'num_in_assignation',
                       'link_to_piece1', 'link_to_piece2', 'link_to_piece3',
                       'link_to_piece4', 'link_to_piece5', 'link_to_piece6']

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.assignation_id:
            return self.readonly_fields +\
                   ['pi_recu', 'doleances_recu', 'cm_recu', 'ord_anc_formule', 'ord_alt', 'hist_tsh', 'divers_recu',
                    'state', 'date_reception', 'arret_travail', 'frais_medicaux']
        else:
            return self.readonly_fields

    def get_dossier_id(self, obj):
        return obj.owner_id
    get_dossier_id.short_description = '# Dossier'
    get_dossier_id.admin_order_field = 'owner_id'

    def get_owner_name(self, obj):
        return obj.owner.get_full_name()
    get_owner_name.short_description = 'Client'
    get_owner_name.admin_order_field = 'owner__nom_de_famille'

    def _pi_recu(self, obj):
        return obj.pi_recu
    _pi_recu.short_description = 'P.Id.'
    _pi_recu.boolean = True

    def _dol_recu(self, obj):
        return obj.doleances_recu
    _dol_recu.short_description = 'Dol.'
    _dol_recu.boolean = True

    def _cm_recu(self, obj):
        return obj.cm_recu
    _cm_recu.short_description = 'C. Med.'
    _cm_recu.boolean = True

    def _hist_tsh(self, obj):
        return obj.hist_tsh
    _hist_tsh.short_description = 'TSH'
    _hist_tsh.boolean = True

    def _divers_recu(self, obj):
        return obj.divers_recu
    _divers_recu.short_description = 'Divers'
    _divers_recu.boolean = True

    def _ord_alt(self, obj):
        return obj.ord_alt
    _ord_alt.short_description = 'Ord. Alt.'
    _ord_alt.boolean = True

    def _ord_anc_formule(self, obj):
        return obj.ord_anc_formule
    _ord_anc_formule.short_description = 'Ord. Anc.'
    _ord_anc_formule.boolean = True

    def link_dossier_to_user(self, obj):
        return mark_safe('<a href="{}">{}</a>'.format(
            reverse("admin:usermod_user_change", args=(obj.owner.id,)),
            obj.owner
        ))
    link_dossier_to_user.short_description = "Client"


admin.site.register(Dossier, DossierAdmin)


class FactureAdmin(admin.ModelAdmin):
    fields = ('id', 'link_to_user', 'date_creation', 'link_to_file_admin')
    list_display = ('id', 'link_to_user', 'date_creation', 'link_to_file_admin')
    search_fields = ('owner__email', 'owner__nom_de_famille', 'owner__prenom', '=id', '=owner__id')
    readonly_fields = ('id', 'link_to_user', 'date_creation', 'link_to_file_admin')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Facture, FactureAdmin)


class DossierInline(admin.TabularInline):
    model = Dossier
    extra = 0
    show_change_link = True
    fields = ['owner', 'owner_id', 'num_in_assignation', 'pi_recu', 'doleances_recu', 'cm_recu',
              'ord_anc_formule', 'ord_alt', 'hist_tsh', 'divers_recu']
    readonly_fields = ['owner', 'pi_recu', 'doleances_recu', 'cm_recu', 'ord_anc_formule',
                       'ord_alt', 'hist_tsh', 'divers_recu', 'num_in_assignation', 'owner_id']
    ordering = ('owner__nom_de_famille',)
    can_delete = True
    max_num = 0


class AssignationAdmin(admin.ModelAdmin):
    fields = (('assignation_id', 'dossier_count'), ('date_envoi', 'closed', 'envoye'),
              ('link_list_plaignant', 'link_csv', 'link_non_produites', 'link_bordereau'))
    list_display = ('assignation_id', 'date_envoi', 'closed', 'envoye', 'dossier_count')
    readonly_fields = ('dossier_count', 'link_list_plaignant',
                       'link_bordereau', 'link_non_produites', 'link_csv')
    inlines = [DossierInline, ]

    def get_readonly_fields(self, request, obj=None):
        if obj:  # This is the case when obj is already created i.e. it's an edit
            return self.readonly_fields + ('assignation_id',)
        else:
            return self.readonly_fields


admin.site.register(Assignation, AssignationAdmin)


@staff_member_required
def get_list_plaignants(request, assignation_id):
    users_in_assignation = User.objects.filter(dossier__assignation_id=assignation_id).order_by('nom_de_famille')
    return render(request, 'list_plaignants.html', {'users': users_in_assignation})


@staff_member_required
def get_bordereau(request, assignation_id):
    dossiers = Dossier.objects.filter(assignation_id=assignation_id).order_by('owner__nom_de_famille')
    return render(request, 'bordereau.html', {'dossiers': dossiers})


@staff_member_required
def get_pieces_non_produites(request, assignation_id):
    dossiers = Dossier.objects.filter(assignation_id=assignation_id).order_by('owner__nom_de_famille')
    return render(request, 'non_produites.html', {'dossiers': dossiers})


@staff_member_required
def show_pieces_manquantes(request):
    incomplete_ids = [dossier.owner_id for dossier in
                      Dossier.objects.filter(assignation_id__gt=15)
                      if not dossier.is_complet()]
    ds = Dossier.objects.filter(owner_id__in=incomplete_ids).order_by('assignation_id', 'num_in_assignation')
    return render(request, 'pieces_manquantes.html', {'dossiers': ds})


@staff_member_required
def show_pieces_manquantes_pas_vus(request):
    incomplete_ids = [dossier.owner_id for dossier in
                      Dossier.objects.filter(assignation_id__gt=15).filter(date_reception__isnull=False)
                      if not dossier.has_dir()]
    ds = Dossier.objects.filter(owner_id__in=incomplete_ids).order_by('assignation_id', 'num_in_assignation')
    return render(request, 'pieces_manquantes.html', {'dossiers': ds})


@staff_member_required
def show_pieces_manquantes_vus(request):
    incomplete_ids = [dossier.owner_id for dossier in
                      Dossier.objects.filter(assignation_id__gt=15).filter(date_reception__isnull=False)
                      if dossier.has_dir() and not dossier.is_complet()]
    ds = Dossier.objects.filter(owner_id__in=incomplete_ids).order_by('assignation_id', 'num_in_assignation')
    return render(request, 'pieces_manquantes.html', {'dossiers': ds})


@staff_member_required
def resume_manquants(request):
    assignations = Assignation.objects.filter(assignation_id__gt=15).order_by('assignation_id')
    return render(request, 'resume_manquants.html', {'assignations': assignations})


@staff_member_required
def plaignants_csv(request, assignation_id):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="plaignants assignation {0}.csv'.format(assignation_id)
    response.write(codecs.BOM_UTF8)
    write_plaignants_csv(response, assignation_id)
    return response


@staff_member_required
def get_facture_admin(request, facture_id):
    facture = Facture.objects.get(pk=facture_id)
    response = HttpResponse()
    response['Content-Type'] = 'application/pdf'
    response["Content-Disposition"] = "inline; filename={0}".format(facture.get_filename())
    response['X-Accel-Redirect'] = facture.get_rel_path()
    return response


@staff_member_required
def get_piece(request, owner_id, pid):
    dossier = Dossier.objects.get(pk=owner_id)
    response = HttpResponse()
    response['Content-Type'] = 'application/pdf'
    response['Title'] = 'Test'
    response["Content-Disposition"] = "inline; filename={0}.{1}.pdf".format(owner_id, pid)
    response['X-Accel-Redirect'] = dossier.get_piece_rel_path(pid)
    return response
