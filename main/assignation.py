#!/usr/bin/env python
# -*- coding: utf-8 -*-
from main.models import Dossier
from main.models import Assignation
from main.stamping import stamp_piece
from main.dossier import get_max_num_in_assignation_id
from django.conf import settings
import os.path
import re
import os
import shutil
import glob
import csv
import logging

logger = logging.getLogger(__name__)


def write_plaignants_csv(file_obj, assignation_id):
    dossiers = Dossier.objects.filter(assignation_id=assignation_id).order_by('num_in_assignation')

    writer = csv.writer(file_obj)
    writer.writerow(['No Plaignant', 'Prénom', 'Nom de Famille', 'Adresse', 'Code Postal', 'Ville',
                     'Date de naissance', 'Lieu de naissance', 'Nationalité', 'Profession'])
    for dossier in dossiers:
        writer.writerow([dossier.num_in_assignation, dossier.owner.prenom, dossier.owner.nom_de_famille.upper(),
                         dossier.owner.adresse, dossier.owner.code_postal, dossier.owner.ville.title(),
                         dossier.owner.date_de_naissance, dossier.owner.lieu_de_naissance.title(),
                         dossier.owner.nationalite.title(), dossier.owner.profession.title()])


def gen_plaignants_csv_file(assignation_id):
    assignation = Assignation.objects.get(assignation_id=assignation_id)
    with open(os.path.join(assignation.get_dir(), 'plaignants CIV {0}.csv'.format(assignation_id)),
              'w', newline='', encoding='utf-8') as csvfile:
        # for BOM encoding
        csvfile.write('\ufeff')
        write_plaignants_csv(csvfile, assignation_id)


def set_num_assignation(assignation_id):
    start_id = get_max_num_in_assignation_id() + 1
    assignation = Assignation.objects.get(assignation_id=assignation_id)
    dossiers = assignation.dossier_set.all().order_by('owner__nom_de_famille')
    print(dossiers.count())
    for dossier in dossiers:
        dossier.num_in_assignation = start_id
        start_id += 1
        dossier.save()


def reset_num_assignation(assignation_id):
    assignation = Assignation.objects.get(assignation_id=assignation_id)
    dossiers = assignation.dossier_set.all()
    print(dossiers.count())
    for dossier in dossiers:
        dossier.num_in_assignation = None
        dossier.save()


def clear_assignation(assignation_id):
    assignation = Assignation.objects.get(assignation_id=assignation_id)
    dossiers = assignation.dossier_set.all()
    print("removing {} dossiers from assignation {}".format(dossiers.count(), assignation_id))
    for dossier in dossiers:
        dossier.assignation_id = None
        dossier.num_in_assignation = None
        dossier.state = 'B'
        new_dossier_path = os.path.join(settings.DOSSIERS_VALIDES_PATH, str(dossier.owner_id))
        print('moving back dossier from {} to {}'.format(dossier.get_dir(), new_dossier_path))
        if os.path.isdir(dossier.get_dir()):
            os.rename(dossier.get_dir(), new_dossier_path)
        else:
            print("Directory {} not found.".format(dossier.get_dir()))
        dossier.save()


def create_assignation_folders(assignation_id):
    assignation = Assignation.objects.get(assignation_id=assignation_id)
    dossiers = assignation.dossier_set.all()
    # delete and recreate assignation dir
    if os.path.isdir(assignation.get_dir()):
        logger.warn("{} exists.".format(assignation.get_dir()))
        # shutil.rmtree(assignation.get_dir())
    else:
        os.makedirs(assignation.get_dir())
    os.makedirs(assignation.get_pieces_dir())
    # os.makedirs(assignation.get_procedure_dir())

    for dossier in dossiers:
        name_strict = re.compile(r'({})\.(?P<pid>[1-6]).pdf$'.format(dossier.owner_id))
        shorcut_name = re.compile(r'django_dossier_({})\.url$'.format(dossier.owner_id))
        if os.path.isdir(dossier.get_dir()):
            # create new dir if not there already
            if not os.path.isdir(dossier.get_assignation_dir()):
                os.makedirs(dossier.get_assignation_dir())
            for filename in os.listdir(dossier.get_dir()):
                ns = name_strict.search(filename)
                if ns:
                    piece_id = ns.group('pid')
                    new_name = 'A.{}.{}.pdf'.format(dossier.num_in_assignation, piece_id)
                    shutil.copyfile(
                        os.path.join(dossier.get_dir(), filename),
                        os.path.join(dossier.get_assignation_dir(), new_name))
                else:
                    sn = shorcut_name.search(filename)
                    if sn or filename == 'NP':
                        pass
                    else:
                        print("{} not copied.".format(filename))
        else:
            print("Il manque le dossier {} pour l'assignation {}".format(dossier.owner_id, assignation_id))

    assignation.write_num_of_pages()


def stamp_assignation(assignation_id):
    assignation = Assignation.objects.get(assignation_id=assignation_id)
    dossiers = assignation.dossier_set.all().order_by('num_in_assignation')

    for dossier in dossiers:
        if dossier.has_dir():
            for filename in os.listdir(dossier.get_assignation_dir()):
                print('stamping ' + os.path.join(dossier.get_assignation_dir(), filename))
                stamp_piece(os.path.join(dossier.get_assignation_dir(), filename))


def set_assignation_from_folder(folder_path, ass_id):
    for folder in os.listdir(folder_path):
        print(folder)
        dossier = Dossier.objects.get(pk=folder)
        dossier.assignation_id = ass_id
        dossier.save()


def rename_pieces_A(path, dry_run=False):
    mat = re.compile(r'(?P<gid>\d+)\.(?P<pid>\d+).pdf$')
    for filepath in glob.glob('E:/*/*/*.pdf'):
        m = mat.search(filepath)
        if m:
            new_filepath = mat.sub(r'A.\g<gid>.\g<pid>.pdf', filepath, 1)
            if dry_run:
                print("Will rename {} to {} ".format(filepath, new_filepath))
            else:
                os.rename(filepath, new_filepath)
