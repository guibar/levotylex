from django.db import models
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse
from usermod.models import User
from main.zendesk import send_dossier_ack_zd
from easy_pdf import rendering
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.conf import settings
import cgi
import os.path
from subprocess import call
from pdfrw import PdfReader, PdfWriter
import glob

max_dossier_in_assignation = 100


class Assignation(models.Model):
    assignation_id = models.IntegerField(_("Num d'assignation"), unique=True, null=False)
    date_envoi = models.DateField(
        _("Date (estimée) d'envoi de l'assignation"), blank=True, null=True, default=None)
    envoye = models.BooleanField(_("Assignation envoyée"), default=False)
    closed = models.BooleanField(_("Assignation close"), default=False)

    class Meta:
        verbose_name = _('assignation')

    def __str__(self):
        return "Assignation " + str(self.assignation_id)

    def link_to_change_page(self):
        link = reverse("admin:main_assignation_change", args=[self.assignation_id])
        return mark_safe('<a href="{}">{}</a>'.format(link, self.id))
    link_to_change_page.short_description = "Assig."

    def get_dir(self):
        return os.path.join(settings.ASSIGNATIONS_PATH, str(self.assignation_id))

    def get_pieces_dir(self):
        return os.path.join(self.get_dir(), 'Pièces')

    def get_procedure_dir(self):
        return os.path.join(self.get_dir(), 'Procédure')

    def dossier_count(self):
        return self.dossier_set.count()
    dossier_count.short_description = "Nombre de dossiers"

    def count_incomplets(self):
        count = 0
        for dossier in self.dossier_set.all():
            if not dossier.is_complet():
                count += 1
        return count

    def count_non_recu(self):
        count = 0
        for dossier in self.dossier_set.all():
            if not dossier.bien_recu():
                count += 1
        return count

    def close_if_full(self):
        if self.dossier_count() >= max_dossier_in_assignation:
            self.closed = True
            self.save()

    def link_list_plaignant(self):
        if self.id:
            link = reverse("list_plaignants", args=[self.id])
            return mark_safe('<a href="{}">{}</a>'.format(link, "Identité des plaignants"))
    link_list_plaignant.short_description = ""

    def link_bordereau(self):
        if self.id:
            link = reverse("bordereau", args=[self.id])
            return mark_safe('<a href="{}">{}</a>'.format(link, "Bordereau des pièces"))
    link_bordereau.short_description = ""

    def link_csv(self):
        if self.id:
            link = reverse("plaignants_csv", args=[self.id])
            return mark_safe('<a href="{}">{}</a>'.format(link, "Plaignants CSV"))
    link_csv.short_description = ""

    def link_non_produites(self):
        if self.id:
            link = reverse("non_produites", args=[self.id])
            return mark_safe('<a href="{}">{}</a>'.format(link, "Pièces non produites"))
    link_non_produites.short_description = ""

    def link_dossier_to_user(self):
        link = reverse("admin:usermod_user_change", args=[self.owner.id])
        return mark_safe('<a href="{}">{}</a>'.format(link, self.owner))
    link_dossier_to_user.short_description = "Client"

    def generate_part_1(self):
        item = ('<text:list-item>\n'
                '    <text:p text:style-name="P33"><text:span text:style-name="T12">{}</text:span>{}</text:p>\n'
                '</text:list-item>\n')
        anchor1 = '<text:list xml:id="list2839849158" text:style-name="WWNum26">'
        anchor2 = '</text:list>'
        anchor = anchor1 + anchor2
        items = anchor1 + '\n'
        users = User.objects.filter(dossier__assignation_id=self.assignation_id).order_by('nom_de_famille')
        for user in users:
            details = ", née le {dn} à {ln}, demeurant {ad}, {cp} {vi}, de nationalité {na}, {pr},\n"\
                .format(dn=user.date_de_naissance,
                        ln=user.lieu_de_naissance.title(),
                        ad=user.adresse,
                        vi=user.ville.title(),
                        cp=user.code_postal,
                        na=user.nationalite,
                        pr=user.profession.title())
            # without the escape , any & in the text will break the fodt
            items += item.format(cgi.escape(user.get_civil_name()), cgi.escape(details))
        items += anchor2

        start_num = 'text:start-value="123456"'
        new_start_num = 'text:start-value="{}"'.format(users[0].dossier.num_in_assignation)

        part_1_fodt_path = os.path.join(self.get_procedure_dir(),
                                        "assignation_{}_part_1.fodt".format(self.assignation_id))
        part_1_pdf_path = os.path.join(self.get_procedure_dir(),
                                       "assignation_{}_part_1.pdf".format(self.assignation_id))
        with open(settings.ASSIGNATION_P1_PATH, "rt") as fin:
            with open(part_1_fodt_path, "w") as fout:
                for line in fin:
                    line1 = line.replace(start_num, new_start_num)
                    line2 = line1.replace(anchor, items)
                    fout.write(line2)
        fin.close()
        fout.close()
        if os.name == 'nt':
            call(["python", settings.UNOCONV_PATH, "-f", "pdf", part_1_fodt_path])
        # linux
        else:
            call([settings.UNOCONV_PATH, "-f", "pdf", part_1_fodt_path])
        os.remove(part_1_fodt_path)
        return part_1_pdf_path

    def generate_part_3(self):
        dossiers = self.dossier_set.order_by('num_in_assignation')
        content = rendering.render_to_pdf('assignation_part_3.html', {'dossiers': dossiers})
        # save pdf to file
        part_3_pdf_path = os.path.join(self.get_procedure_dir(),
                                       "Bordereau_A_CIV_{}_v2.pdf".format(self.assignation_id))
        with open(part_3_pdf_path, 'wb') as save_file:
            save_file.write(content)
        return part_3_pdf_path

    def generate_pdf(self):
        outfn = os.path.join(self.get_procedure_dir(), "Levothyrox_CIV_{}.pdf".format(self.assignation_id))
        writer = PdfWriter()
        p1 = self.generate_part_1()
        p3 = self.generate_part_3()
        writer.addpages(PdfReader(p1).pages)
        writer.addpages(PdfReader(settings.ASSIGNATION_P2_PATH).pages)
        writer.addpages(PdfReader(p3).pages)
        writer.addpages(PdfReader(settings.ASSIGNATION_P4_PATH).pages)
        writer.write(outfn)
#        self.write_num_of_pages()
        os.remove(p1)
        os.remove(p3)

    def write_num_of_pages(self):
        write_num_of_pages_path(self.get_dir())

    def write_num_of_pages_pieces(self):
        write_num_of_pages_path(os.path.join(self.get_dir(), "Pièces"))


def write_num_of_pages_path(path):
    total = 0
    if os.path.isdir(path):
        with open(os.path.join(path, "nb_pages.txt"), "w+") as fout:
            for filepath in glob.iglob(path + '/**/*.pdf', recursive=True):
                nb_pages = len(PdfReader((filepath)).pages)
                total += nb_pages
                fout.write("{}\t{}\n".format(os.path.relpath(filepath, path), nb_pages))
            fout.write("Total\t{}\n".format(total))
        fout.close()


class Dossier(models.Model):
    # I would like to define a label 'Client' for owner
    owner = models.OneToOneField(User, primary_key=True, related_name='dossier', editable=False)
    date_reception = models.DateField(
        _("Date de reception"), blank=True, null=True, default=None)
    assignation = models.ForeignKey(Assignation, on_delete=models.SET_NULL, null=True, blank=True)
    num_in_assignation = models.IntegerField(_("Numéro dans assignation"), unique=True, null=True, blank=True)

    pi_recu = models.BooleanField(_("Pièce identité reçue"), default=False)
    doleances_recu = models.BooleanField(_("Doléances reçues"), default=False)
    hist_tsh = models.BooleanField(_("Historique TSH reçu"), default=False)
    cm_recu = models.BooleanField(_("Certificat médical reçu"), default=False)
    ord_anc_formule = models.BooleanField(_("Ordonnance Euthyrox reçue"), default=False)
    ord_alt = models.BooleanField(_("Ordonnance Alternative reçue"), default=False)
    divers_recu = models.BooleanField(_("Autre(s) pièce(s) reçue(s)"), default=False)
    arret_travail = models.IntegerField(_("Jours arrêt de travail"), null=True, blank=True)
    frais_medicaux = models.IntegerField(_("Montant frais médicaux"), null=True, blank=True)
    commentaires = models.TextField(_("Commentaires"), max_length=300, default="", blank=True)

    STATES = (('W', 'Attendu'), ('T', 'En prep.'), ('V', 'À valider'), ('B', 'Validé'),
              ('A', 'Assigné'), ('I', 'Incomplet'), ('R', 'Refusé'))
    state = models.CharField(_("Status"), max_length=1, choices=STATES, null=False, default='W')

    date_ack_sent = models.DateTimeField(_("Date d'envoi de l'accusé de réception"), null=True)
    zendesk_id = models.BigIntegerField(_("Ticket Zendesk associé"), null=True)

    class Meta:
        verbose_name = _('dossier')

    # if the ack hasn't been sent and something has been received, send an ack
    def save(self, *args, **kwargs):
        if self.date_ack_sent is None and self.date_reception and \
           (self.pi_recu or self.doleances_recu or self.hist_tsh
                or self.cm_recu or self.ord_anc_formule or self.ord_alt):
            self.state = 'T'
            send_dossier_ack_zd(self)
            self.date_ack_sent = timezone.now()
        super(Dossier, self).save(*args, **kwargs)

    def __str__(self):
        return str("Dossier " + str(self.owner.id))

    def link_to_assignation(self):
        if self.assignation:
            link = reverse("admin:main_assignation_change", args=[self.assignation.assignation_id])
            return mark_safe('<a href="{}">{}</a>'.format(link, self.assignation.id))
        else:
            return "-"
    link_to_assignation.short_description = "Assig."

    def link_to_dossier(self):
        link = reverse("admin:main_dossier_change", args=[self.owner.id])
        return mark_safe('<a href="{}">{}</a>'.format(link, self.owner.id))
    link_to_dossier.short_description = "Dossier"

    def link_to_zendesk(self):
        if self.zendesk_id:
            link = "https://mysmartcab.zendesk.com/agent/tickets/{}".format(self.zendesk_id)
            return mark_safe('<a href="{}" target="_blank">{}</a>'.format(link, self.zendesk_id))
        else:
            return "-"
    link_to_zendesk.short_description = "Ticket Zendesk du dossier"

    def bien_recu(self):
        return self.date_reception and (
               self.pi_recu or self.doleances_recu or self.hist_tsh
               or self.cm_recu or self.ord_anc_formule or self.ord_alt)

    def get_dir(self):
        return os.path.join(settings.DOSSIERS_PATH, str(self.owner.id))

    def has_dir(self):
        return os.path.isdir(self.get_dir())

    def missing_all_pieces(self):
        return self.date_reception and not self.has_dir()

    def get_assignation_dir(self):
        if self.num_in_assignation:
            return os.path.join(self.assignation.get_pieces_dir(), str(self.num_in_assignation))
        else:
            return None

    def piece_recue(self, i):
        if i == 1:
            return self.pi_recu
        elif i == 2:
            return self.doleances_recu
        elif i == 3:
            return self.cm_recu
        elif i == 4:
            return self.has_ord()
        elif i == 5:
            return self.hist_tsh
        elif i == 6:
            return self.divers_recu
        else:
            return None

    def get_pieces_set(self):
        result = set()
        if self.pi_recu:
            result.add(1)
        if self.doleances_recu:
            result.add(2)
        if self.cm_recu:
            result.add(3)
        if self.has_ord():
            result.add(4)
        if self.hist_tsh:
            result.add(5)
        # ignore piece 6 at this stage
        # if self.divers_recu:
            # result.add(6)
        return result

    def has_piece(self, i):
        return os.path.isfile(os.path.join(self.get_dir(), '%d.%d.pdf' % (self.owner.id, i)))

    def has_piece_NP(self, i):
        return os.path.isfile(os.path.join(self.get_dir(), 'NP/%d.%d.pdf' % (self.owner.id, i)))

    def has_ord(self):
        return self.ord_alt or self.ord_anc_formule
    has_ord.short_description = "A une ordonnance"
    has_ord.boolean = True

    def pi_prod(self):
        return self.has_piece(1)
    pi_prod.short_description = "P.I. produite"
    pi_prod.boolean = True

    def dol_prod(self):
        return self.has_piece(2)
    dol_prod.short_description = "C.Dol. produit"
    dol_prod.boolean = True

    def cm_prod(self):
        return self.has_piece(3)
    cm_prod.short_description = "C.M. produit"
    cm_prod.boolean = True

    def ord_prod(self):
        return self.has_piece(4)
    ord_prod.short_description = "Ord. produite"
    ord_prod.boolean = True

    def tsh_prod(self):
        return self.has_piece(5)
    tsh_prod.short_description = "TSH produite"
    tsh_prod.boolean = True

    # if there is no dossier dir, say there is no P6
    def div_prod(self):
        return self.has_piece(6)
    div_prod.short_description = "Div. produits"
    div_prod.boolean = True

    # if there is no dossier dir, say there is no P6
    def is_complet(self):
        for i in range(1, 6):
            if self.piece_recue(i) != self.has_piece(i):
                return False
        return True
    div_prod.short_description = "Dossier complet"
    div_prod.boolean = True

    def get_piece_rel_path(self, pid):
        return os.path.join(settings.REL_DOSSIERS_PATH, '%d/%d.%s.pdf' % (self.owner.id, self.owner_id, pid))

    def link_to_piece1(self):
        if self.has_piece(1):
            link = reverse("piece", args=[self.owner_id, 1])
            return mark_safe('<a href="{}">{}</a>'.format(link, 'Piece 1'))
        else:
            return "-"
    link_to_piece1.short_description = ""

    def link_to_piece2(self):
        if self.has_piece(2):
            link = reverse("piece", args=[self.owner_id, 2])
            return mark_safe('<a href="{}">{}</a>'.format(link, 'Piece 2'))
        else:
            return "-"
    link_to_piece2.short_description = ""

    def link_to_piece3(self):
        if self.has_piece(3):
            link = reverse("piece", args=[self.owner_id, 3])
            return mark_safe('<a href="{}">{}</a>'.format(link, 'Piece 3'))
        else:
            return "-"
    link_to_piece3.short_description = ""

    def link_to_piece4(self):
        if self.has_piece(4):
            link = reverse("piece", args=[self.owner_id, 4])
            return mark_safe('<a href="{}">{}</a>'.format(link, 'Piece 4'))
        else:
            return "-"
    link_to_piece4.short_description = ""

    def link_to_piece5(self):
        if self.has_piece(5):
            link = reverse("piece", args=[self.owner_id, 5])
            return mark_safe('<a href="{}">{}</a>'.format(link, 'Piece 5'))
        else:
            return "-"
    link_to_piece5.short_description = ""

    def link_to_piece6(self):
        if self.has_piece(6):
            link = reverse("piece", args=[self.owner_id, 6])
            return mark_safe('<a href="{}">{}</a>'.format(link, 'Piece 6'))
        else:
            return "-"
    link_to_piece6.short_description = ""


class Facture(models.Model):
    id = models.AutoField(primary_key=True, verbose_name="Numero de facture")
    owner = models.OneToOneField(User, verbose_name="Client")
    date_creation = models.DateTimeField(
        _("Date d'envoi de la facture"))

    class Meta:
        verbose_name = _('facture')

    def get_filename(self):
        padded_id = '%04d' % self.id
        return 'facture_' + str(self.owner.id) + '_' + padded_id + '.pdf'

    def get_path(self):
        return os.path.join(settings.FACTURES_PATH, self.get_filename())

    def get_rel_path(self):
        return os.path.join(settings.REL_FACTURES_PATH, self.get_filename())

    def link_to_user(self):
        link = reverse("admin:usermod_user_change", args=[self.owner.id])
        return mark_safe('<a href="{}">{}</a>'.format(link, self.owner))
    link_to_user.short_description = 'Client'

    def link_to_file_admin(self):
        if self.id:
            link = reverse("facture_admin", args=[self.id])
            return mark_safe('<a href="{}">{}</a>'.format(link, self.get_filename()))
    link_to_file_admin.short_description = "Fichier PDF"
