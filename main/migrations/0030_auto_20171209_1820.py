# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-09 17:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0029_auto_20171209_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dossier',
            name='state',
            field=models.CharField(choices=[('W', 'En attente'), ('T', 'En traitement'), ('V', 'À valider'), ('B', 'Validé'), ('A', 'Assigné'), ('I', 'Incomplet'), ('R', 'Refusé')], default='W', max_length=1),
        ),
    ]
