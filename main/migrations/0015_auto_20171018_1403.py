# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-18 12:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20171018_1141'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='analysetsh',
            options={'verbose_name': 'Analyse'},
        ),
        migrations.RemoveField(
            model_name='dossier',
            name='valide',
        ),
        migrations.AddField(
            model_name='dossier',
            name='cm_recu',
            field=models.BooleanField(default=False, verbose_name='Certificat médical reçu'),
        ),
        migrations.AlterField(
            model_name='dossier',
            name='doleances_recu',
            field=models.BooleanField(default=False, verbose_name='Doléances reçues'),
        ),
    ]
