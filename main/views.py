from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes, force_text
from django.http import HttpResponse
from main.forms import ProfileForm, SignInForm
from usermod.models import User
import simplejson as json
from django.db.models import Q
from django.contrib.auth.tokens import PasswordResetTokenGenerator

token_generator = PasswordResetTokenGenerator()


def signin(request):
    if request.method == 'POST':
        form = SignInForm(request.POST)
        if form.is_valid():
            # user exists -> signin
            try:
                user = User.objects.get(email=form.cleaned_data['email'])
            # user doesn't exit -> signup
            except ObjectDoesNotExist:
                form.cleaned_data
                user = User(email=form.cleaned_data['email'])
                user.date_inscription = timezone.now()
                # user = form.save(commit=False)
                user.is_active = False
                user.save()
            current_site = get_current_site(request)
            subject = "Acceder à votre compte MySMARTCab-Levothyrox"
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': token_generator.make_token(user),
            })
            user.email_user(subject, message, 'Equipe MySmartCab <levo@mysmartcab.fr>')
            return redirect('account_activation_sent', user.email)
    else:
        form = SignInForm()
    return render(request, 'signin.html', {'form': form})


@login_required
def profile(request):
    if request.method == "POST":
        form = ProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            user = form.save(commit=False)
            user.save()
            return render(request, 'profile.html', {'form': form})
    else:
        # if coming back from pz and session has timed out
        # check that email and trans_id make sense and log them in
        # difficult to test, hence put something in logs to show we have been here
        # TODO add some special visual effect to notify of payment just received
        # if 'pz' in request.GET and not request.user.is_authenticated():
            # trans_id = request.GET['vads_trans_id']
            # email = request.GET['vads_cust_email']
            # logger.info("User {} coming back from payzen not authenticated".format(email))
            # try:
                # user = User.objects.get(email=email)
                # if (user.trans_id == trans_id):
                    # user = authenticate(username=email, password="")
                    # login(request, user)
            # # user doesn't exit at all ... bizarre!!!
            # except ObjectDoesNotExist:
                # logger.critical("Upon returning from shop, email {} cannot be connected to a valid user".format(email)) # noqa: E501
                # return HttpResponse('')
        form = ProfileForm(instance=request.user)
    return render(request, 'profile.html', {'form': form})


def cgu(request):
    """Page des CGU"""
    return render(request, 'cgu.html')


def ch(request):
    """Page de la convention d'honoraires"""
    return render(request, 'ch.html')


def apropos(request):
    return render(request, 'apropos.html')


def faq(request):
    return render(request, 'faq.html')


def account_activation_sent(request, email='votre adresse'):
    return render(request, 'account_activation_sent.html', {'email': email})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        user.is_active = True
        user.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('profile')
    else:
        return render(request, 'account_activation_invalid.html')


@login_required
def get_facture(request):
    response = HttpResponse()
    response['Content-Type'] = 'application/pdf'
    response["Content-Disposition"] = "inline; filename={0}".format(
            request.user.facture.get_filename())
    response['X-Accel-Redirect'] = request.user.facture.get_rel_path()
    return response


@login_required
def get_ch(request):
    response = HttpResponse()
    response['Content-Type'] = 'application/pdf'
    response["Content-Disposition"] = "inline; filename={0}".format(
            request.user.get_ch_filename())
    response['X-Accel-Redirect'] = request.user.get_rel_ch_path()
    return response


def counter(request):
    nb_inscrits = User.objects.filter(
            Q(email_confirmed=True) |
            Q(payment_par_cheque=True) |
            Q(payment_par_virement=True)).count()
    data = {
        'nb_inscrits': nb_inscrits,
    }
    return HttpResponse(json.dumps(data), content_type='application/json')
