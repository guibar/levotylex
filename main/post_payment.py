import base64
import os
import logging
import pytz
import dateutil.parser
from django.conf import settings
from django.template.defaultfilters import date as _date
from django.utils.timezone import now as tznow
from django.apps import apps
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils import timezone

from easy_pdf import rendering
from mailin import Mailin
from main.zendesk import create_or_update_dossier_ticket, create_post_invoice_ticket
from levotylex.delayed_jobs import postpone

logger = logging.getLogger(__name__)


@postpone
def post_payment(user):
    generate_facture_and_send(user)
    create_or_update_dossier_ticket(user)


def generate_facture_and_send(user, fact_gap_id=None, date_cr=None):
    # just to handle easily regeneration of facture
    # this will not happen in normal operation
    Facture = apps.get_model('main', 'Facture')
    try:
        facture = user.facture
    except Facture.DoesNotExist:
        # if an id has been passed, use it
        if fact_gap_id:
            if date_cr:
                d_aware = pytz.utc.localize(dateutil.parser.parse(date_cr))
                facture = Facture(id=fact_gap_id, owner=user, date_creation=d_aware)
            else:
                facture = Facture(id=fact_gap_id, owner=user, date_creation=tznow())
        # otherwise get the next id automatically
        else:
            facture = Facture(owner=user, date_creation=tznow())
        facture.save()
    # generate pdf content
    f_date = _date(facture.date_creation, "d F Y")
    # id padded to 4 numbers
    fact_id = '%04d' % facture.id
    content = rendering.render_to_pdf('facture_pdf.html',
                                      {'user': user, 'facture_id': fact_id, 'date': f_date})
    # save pdf to file
    with open(facture.get_path(), 'wb') as save_file:
        save_file.write(content)
    # send facture with pieces or just facture
    if user.payment_received:
        send_pieces_and_facture(user, facture)
    elif user.email_confirmed:
        send_facture(user, facture)
    else:
        create_post_invoice_ticket(user)


def send_pieces_and_facture(user, facture):
    with open(facture.get_path(), "rb") as file:
        base64_bytes_facture = base64.b64encode(file.read())
    base64_string_facture = base64_bytes_facture.decode('utf-8')

    with open(settings.PIECES_FILE_PATH, "rb") as file:
        base64_bytes_pieces = base64.b64encode(file.read())
    base64_pieces_string = base64_bytes_pieces.decode('utf-8')

    m = Mailin("https://api.sendinblue.com/v2.0", "CZJYrgjP1O5sW7vh")
    # envoi pièces et facture
    data = {"id": 58,
            "to": user.email,
            "headers": {"Content-Type": "text/html;charset=iso-8859-1"},
            "attachment": {"pieces_pour_dossier.pdf": base64_pieces_string,
                           os.path.basename(facture.get_path()): base64_string_facture},
            }
    result = m.send_transactional_template(data)
    if (result['code'] == 'success'):
        user.date_envoi_pieces = tznow()
        user.save()
        logger.info("Les pieces et la facture ont été envoyées à l'utilisateur {}".format(str(user.id)))
    else:
        logger.info("Echec dans l'envoi des pieces et de la facture à l'utilisateur {}".format(str(user.id)))
    return


def send_facture(user, facture):
    with open(facture.get_path(), "rb") as file:
        base64_bytes_facture = base64.b64encode(file.read())
    base64_string_facture = base64_bytes_facture.decode('utf-8')

    m = Mailin("https://api.sendinblue.com/v2.0", "CZJYrgjP1O5sW7vh")
    data = {"id": 65,
            "to": user.email,
            "headers": {"Content-Type": "text/html;charset=iso-8859-1"},
            "attachment": {os.path.basename(facture.get_path()): base64_string_facture},
            }
    result = m.send_transactional_template(data)
    if (result['code'] == 'success'):
        # using this as a placeholder for facture as well
        user.date_envoi_pieces = tznow()
        user.save()
        logger.info("La facture a été envoyée à l'utilisateur {}".format(str(user.id)))
    else:
        logger.info("Echec dans l'envoi de la facture à l'utilisateur {}".format(str(user.id)))
    return


@postpone
def build_ch_send_ch_cgu(user, template):
    """Method to generate a PDF for CH
       Receives an user object and a template file"""
    date_envoi = timezone.now()
    ch_pdf_vars = {'user': user, 'date_envoi': date_envoi}
    content = rendering.render_to_pdf(template, ch_pdf_vars)
    with open(user.get_ch_path(), 'wb') as save_file:
        save_file.write(content)
    user.date_envoi_ch = date_envoi
    user.save()
    message_body = render_to_string('email_cgu_ch.html')
    message = EmailMessage(
        "Documents contractuels MySMARTCab-Action Levothyrox",
        message_body,
        'Equipe MySmartCab <levo@mysmartcab.fr>',
        [user.email],
        [],
        reply_to=['levo@mysmartcab.fr'],
        headers={'Message-ID': 'foo'},
    )
    message.attach_file(user.get_ch_path())
    message.attach_file(settings.CGU_FILE_PATH)
    message.send(fail_silently=False)


def _build_cgu_pdf():
    content = rendering.render_to_pdf('cgu_pdf.html', {})
    with open(settings.CGU_FILE_PATH, 'wb') as save_file:
        save_file.write(content)
