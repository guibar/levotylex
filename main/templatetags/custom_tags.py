from django import template
from usermod.models import User
from django.db.models import Q

register = template.Library()


@register.simple_tag
def nb_inscrits():
    return User.objects.filter(
                Q(email_confirmed=True) |
                Q(payment_par_cheque=True) |
                Q(payment_par_virement=True)).count()


@register.filter
def boolean_icon(value):
    if value:
        return 'admin/img/icon-yes.svg'
    else:
        return 'admin/img/icon-no.svg'
