from main.models import Dossier
from main.models import Assignation
from django.conf import settings
from django.db.models import Max
import os.path
import re
import os
import shutil


def get_current_assignation():
    results = Assignation.objects.filter(closed=False).order_by('assignation_id')
    if not results:
        max_id = Assignation.objects.all().aggregate(Max('assignation_id')).get('assignation_id__max')
        new_id = max_id + 1
        print('creating new assignation with id {}'.format(new_id))
        return Assignation.objects.create(assignation_id=new_id)
    else:
        return results.first()


def get_max_num_in_assignation_id():
    max = Dossier.objects.all().aggregate(Max('num_in_assignation'))
    if max['num_in_assignation__max']:
        return max['num_in_assignation__max']
    else:
        return 0


def check_sub_folders(path, dry_run=False):
    for folder in os.listdir(path):
        # print(folder)
        check_folder(os.path.join(path, folder), dry_run)


def check_folder(folder_path, dry_run):
    msgs = []
    dossier = get_dossier_from_folder(folder_path, msgs)
    if dossier:
        write_shortcut_to_dossier(folder_path, dossier.owner_id)
        if not (dossier.assignation_id and dossier.assignation_id > 15):
            msgs.append("Le dossier {} n'appartient pas à une assignation à completer.\n".format(dossier.owner_id))
        if os.path.isdir(dossier.get_dir()):
            msgs.append("Le dossier {} a déjà un dossier sur le serveur. S'il s'agit d'un complément de pièces,"
                        "mettre ce dossier dans XXX.".format(dossier.owner_id))
        pieces_found, pieces_found_prod = find_pieces(folder_path, dry_run, msgs)
    if msgs:
        write_errors(folder_path, dry_run, msgs)
        return None
    else:
        return dossier


def check_folder_complete(folder_path, dry_run):
    msgs = []
    dossier = get_dossier_from_folder(folder_path, msgs)
    if dossier:
        write_shortcut_to_dossier(folder_path, dossier.owner_id)
        if not (dossier.assignation_id and dossier.assignation_id > 15):
            msgs.append("Le dossier {} n'appartient pas à une assignation à completer.\n".format(dossier.owner_id))
        if os.path.isdir(dossier.get_dir()):
            msgs.append("Le dossier {} a déjà un dossier sur le serveur. S'il s'agit d'un complément de pièces,"
                        "mettre ce dossier dans XXX.".format(dossier.owner_id))
        pieces_found, pieces_found_prod = find_pieces(folder_path, dry_run, msgs)
        diff_pieces_with_django(dossier, pieces_found, pieces_found_prod, msgs)
    if msgs:
        write_errors(folder_path, dry_run, msgs)
        return None
    else:
        return dossier


# associate a folder to a dossier object if possible
def get_dossier_from_folder(folder_path, msgs):
    parent, dir_name = os.path.split(folder_path)

    if dir_name.endswith('.pb'):
        return None
    exc = False
    try:
        id = int(dir_name)
        dossier = Dossier.objects.get(pk=id)
    except ValueError:
        exc = True
        msgs.append("Le dossier '{}' a un nom incorrect car il ne contient pas uniquement des chiffres.\n"
                    .format(dir_name))
    except Dossier.DoesNotExist:
        exc = True
        msgs.append("Le dossier '{}' ne correspond à aucun dossier connu dans Django.\n".format(dir_name))
    finally:
        if exc:
            return None
        else:
            return dossier


# do detailed checks once dossier has been found
def check_with_django_1(dossier, folder_path, msgs):
    parent, dir_name = os.path.split(folder_path)
    if not dossier.bien_recu():
        msgs.append("Le dossier {} n'a pas été reçu selon Django.\n".format(dir_name))
    if not dossier.owner.has_payed():
        msgs.append("Le dossier {} n'a pas été payé selon Django.\
                     Ce dossier ne doit donc pas être traité.\n".format(dir_name))
    # check if this folder has been dealt with before ...
    if os.path.isdir(dossier.get_dir()):
        msgs.append("Le dossier {} est déjà présent dans les dossiers.\n"
                    .format(dir_name))
    if dossier.state == 'R':
        msgs.append("Le dossier {} a été refusé selon Django. Ce dossier ne doit donc pas être traité.\n"
                    .format(dir_name))


# find the pieces in the folder and flag it if it contains errors
def find_pieces(folder_path, dry_run, msgs):
    parent, dir_name = os.path.split(folder_path)

    name_strict = re.compile(r'({})\.(?P<piece_id>[1-6]).pdf$'.format(dir_name))
    name_loose = re.compile(r'({})(?P<dot_>_|\.)(?P<extra0s>0*)(?P<piece_id>[1-6]).pdf$'.format(dir_name))
    shorcut_name = re.compile(r'django_dossier_({})\.url$'.format(dir_name))

    pieces_found = set()
    pieces_found_prod = set()
    dir_is_np = False
    for dir_path, subdirList, fileList in os.walk(folder_path):
        if dir_path == folder_path:
            dir_is_np = False
        elif dir_path == os.path.join(folder_path, 'NP'):
            dir_is_np = True
        else:
            msgs.append("Le dossier {} ne devrait pas se trouver dans {}.\n".format(dir_path, dir_name))
            continue
        for filename in fileList:
            nl = name_loose.search(filename)
            ns = name_strict.search(filename)
            sn = shorcut_name.search(filename)
            # a piece should respect the loose naming convention
            if nl:
                # the name is not strict, needs renaming
                if not ns:
                    if filename == dir_name + '_1.pdf':
                        if dry_run:
                            print('remove ' + os.path.join(dir_path, filename))
                        else:
                            os.remove(os.path.join(dir_path, filename))
                    else:
                        new_filename = name_loose.sub(r'{}.\g<piece_id>.pdf'.format(dir_name), filename, 1)
                        if dry_run:
                            print("Will rename {} to {}.\n"
                                  .format(os.path.join(dir_path, filename), os.path.join(dir_path, new_filename)))
                        else:
                            os.rename(os.path.join(dir_path, filename), os.path.join(dir_path, new_filename))
                # ignore p6 at this stage
                if not nl.group('piece_id') == '6':
                        pieces_found.add(int(nl.group('piece_id')))
                        # if we are in the main folder
                        if not dir_is_np:
                            pieces_found_prod.add(int(nl.group('piece_id')))
            elif sn:
                pass
            else:
                msgs.append("Le fichier {} ne devrait pas être présent dans {}.\n".format(filename, dir_name))
    return pieces_found, pieces_found_prod


def diff_pieces_with_django(dossier, pieces_found, pieces_found_prod, msgs):
    missing = dossier.get_pieces_set() - pieces_found
    # ignore undeclared for now
# if found_undeclared:
    # msgs.append(get_set_messages("La pièce {} du dossier {} n'est pas déclarée dans django.\n",
    # found_undeclared, dossier.owner_id))
    if missing:
        msgs.append(get_set_messages("La pièce {} du dossier {} déclarée dans django n'a pas été trouvée.\n",
                                     missing, dossier.owner_id))

# if not dossier.pi_recu:
        # msgs.append("Le dossier {} n'a pas déclaré de pièce d'identité.\n".format(dossier.owner_id))
# elif 1 not in pieces_found_prod:
        # msgs.append("La pièce d'identité du dossier {} doit être produite.\n".format(dossier.owner_id))
# if not dossier.doleances_recu:
        # msgs.append("Le dossier {} n'a pas déclaré de cahier de doléances.\n".format(dossier.owner_id))
# elif 2 not in pieces_found_prod:
        # msgs.append("Le cahier de doléances du dossier {} doit être produit.\n".format(dossier.owner_id))
# if not dossier.has_ord() and not dossier.cm_recu:
        # msgs.append("Le dossier {} n'a déclaré ni ordonnance ni CM.\n".format(dossier.owner_id))
# elif 3 not in pieces_found_prod and 4 not in pieces_found_prod:
        # msgs.append("Le CM ou l'ordonnance du dossier {} doivent être produits.\n".format(dossier.owner_id))

# Let pass missing piece 6, on demand from Xavier
# if msgs == "La pièce 6 du dossier {} déclarée dans django n'a pas été trouvée.\n".format(dossier.owner_id):
    # msgs = ""


def write_errors(folder_path, dry_run, msgs):
    if dry_run:
        for m in msgs:
            print(m)
    else:
        file = open(os.path.join(folder_path, 'pb.txt'), 'w')
        for m in msgs:
            file.write(m)
        file.close()
        os.rename(folder_path, folder_path + '.pb')


def get_set_messages(message, set, dossier_id):
    m = ""
    for el in set:
        m += message.format(el, dossier_id)
    return m


def assign_valides():
    for folder in os.listdir(settings.DOSSIERS_VALIDES_PATH):
        assign_dossier(os.path.join(settings.DOSSIERS_VALIDES_PATH, folder))


def assign_dossier(dossier_path):
    dossier = check_folder(dossier_path)
    if dossier:
        new_dossier_path = os.path.join(settings.DOSSIERS_PATH, str(dossier.owner_id))
        if os.path.isdir(new_dossier_path):
            print("Un dossier {} existe déjà sur le serveur.".format(new_dossier_path))
        else:
            assignation = get_current_assignation()
            dossier.assignation = assignation
            dossier.state = 'A'
            print('renaming from {} to {}'.format(dossier_path, new_dossier_path))
            shutil.move(dossier_path, new_dossier_path)
            dossier.save()
            assignation.close_if_full()


def get_dossiers_assignes(dry_run):
    for folder in os.listdir(settings.DOSSIERS_VALIDES_PATH):
        incorporate_dossier(os.path.join(settings.DOSSIERS_VALIDES_PATH, folder), dry_run)


def incorporate_dossier(dossier_path, dry_run):
    dossier = check_folder(dossier_path, dry_run)
    if dossier:
        new_dossier_path = os.path.join(settings.DOSSIERS_PATH, str(dossier.owner_id))
        if dry_run:
            print('renaming from {} to {}'.format(dossier_path, new_dossier_path))
        else:
            # don't use new_dossier_path as destination, if it exists, it will copy inside it.
            # the move will fail if a folder exists already in destination
            shutil.move(dossier_path, settings.DOSSIERS_PATH)


def assign_dossiers_sans_pieces():
    # find all dossier which are state = A but with no assignation_id.
    dossiers = Dossier.objects.filter(state='A').filter(assignation_id__isnull=True)
    print(dossiers.count())
    for dossier in dossiers:
        assignation = get_current_assignation()
        dossier.assignation = assignation
        dossier.state = 'A'
        print('assigning dossier {} to assignation {}'.format(dossier.owner_id, assignation.assignation_id))
        dossier.save()
        assignation.close_if_full()


def write_shortcut_to_dossier(folder_path, id):
    target = "https://levo.mysmartcab.fr/admin/main/dossier/{}/change/".format(id)
    filename = os.path.join(folder_path, "django_dossier_{}.url".format(id))
    shortcut = open(filename, 'w')
    shortcut.write('[InternetShortcut]\n')
    shortcut.write('URL={}'.format(target))
    shortcut.close()
