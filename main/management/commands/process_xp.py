import os
import time
import datetime
from django.core.management.base import BaseCommand
from main.dossier import check_folder, find_pieces, diff_pieces_with_django, write_errors


class Command(BaseCommand):
    help = 'Do basic checks in XP'

    a_valider_path = '/var/dav_levo/a_valider/'
    valides_path = '/var/dav_levo/valides/'
    incomplets_path = '/var/dav_levo/incomplets/'

    def handle(self, *args, **options):
        log_file = '/home/gui/xp.log'
        log_h = open(log_file, 'a')
        log_h.write("Running at {}\n".format(str(datetime.datetime.now())))

        lock_file = os.path.join('/home/gui/xp.lock')
        if os.path.isfile(lock_file):
            log_h.write('lock file detected. exiting ...\n')
            return

        # create lock file
        lock_h = open(lock_file, 'w')
        lock_h.write(str(datetime.datetime.now()))
        lock_h.close()
        for folder in os.listdir(Command.a_valider_path):
            folder_path = os.path.join(Command.a_valider_path, folder)
            # ignore files
            if os.path.isfile(folder_path):
                if folder != 'lock':
                    log_h.write('{} found\n'.format(folder_path))
            # folders
            else:
                time_since_last_mod = time.time() - os.path.getmtime(folder_path)
                if time_since_last_mod > 30:
                    dossier = check_folder(folder_path, False)
                    if dossier:
                        msgs = []
                        pieces_found, pieces_found_prod = find_pieces(folder_path, False, msgs)
                        msgs = []
                        diff_pieces_with_django(dossier, pieces_found, pieces_found_prod, msgs)
                        if msgs:
                            log_h.write("{} is incomplete and will be moved to {} ..."
                                        .format(folder_path, Command.incomplets_path))
                            log_h.flush()
                            write_errors(folder_path, False, msgs)
                            folder = folder + '.pb'
                            folder_path = folder_path + '.pb'
                            destination_path = os.path.join(Command.incomplets_path, folder)
                        else:
                            log_h.write("{} is ok and will be moved to {} ..."
                                        .format(folder_path, Command.valides_path))
                            log_h.flush()
                            destination_path = os.path.join(Command.valides_path, folder)
                        os.rename(folder_path, destination_path)
                        log_h.write(" Done!\n")
                        log_h.flush()
        # delete lock file
        os.remove(lock_file)
        log_h.close()
