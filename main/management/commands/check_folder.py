import os
from django.core.management.base import BaseCommand
from main.dossier import check_folder_complete


class Command(BaseCommand):
    help = 'Check all subfolders to see if they have any errors'

    def add_arguments(self, parser):
        parser.add_argument('folder', type=str)

    def handle(self, *args, **options):
        base_dir = options['folder']
        for folder in os.listdir(base_dir):
            folder_path = os.path.join(base_dir, folder)
            # report files other than lock
            if os.path.isfile(folder_path):
                print('unexpected file {} found\n'.format(folder_path))
            # folders
            else:
                if folder.endswith('.pb'):
                    continue
                else:
                    check_folder_complete(folder_path, True)
