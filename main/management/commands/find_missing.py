import os
from main.models import Dossier
from django.core.management.base import BaseCommand
from django.conf import settings
import subprocess


class Command(BaseCommand):
    help = 'Find missing files'

    def handle(self, *args, **options):
        ssh = subprocess.Popen(['ssh', 'root@cloud.mysmartcab.fr', ' ./get_dossiers_on_cloud'],
                               shell=False,
                               stderr=subprocess.PIPE,
                               stdout=subprocess.PIPE)
        result = ssh.stdout.readlines()
        dossiers_set = set()
        for l in result:
            dossiers_set.add(int(l))

        for folder in os.listdir(settings.DOSSIERS_PATH):
            dossiers_set.add(int(folder))

        django_set = set()
        for d in Dossier.objects.filter(assignation_id__gt=15).filter(date_reception__isnull=False):
            django_set.add(d.owner_id)
        missing_set = django_set - dossiers_set
        for el in missing_set:
            print(el)
