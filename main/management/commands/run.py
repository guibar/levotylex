from django.core.management.base import BaseCommand
from main.models import write_num_of_pages_path


class Command(BaseCommand):
    help = 'Find missing files'

    def handle(self, *args, **options):
        write_num_of_pages_path('/var/levo/assignations')
