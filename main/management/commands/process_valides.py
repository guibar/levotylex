import os
import time
import datetime
from django.core.management.base import BaseCommand
from django.conf import settings
from main.dossier import incorporate_dossier


class Command(BaseCommand):
    help = 'Deal with dossiers valides on cloud'

    def handle(self, *args, **options):
        log_file = '/home/gui/valides.log'
        log_h = open(log_file, 'a')
        log_h.write("Running at {}\n".format(str(datetime.datetime.now())))

        lock_file = '/home/gui/valides.lock'
        if os.path.isfile(lock_file):
            log_h.write('lock file detected. exiting ...\n')
            return

        # create lock file
        lock_h = open(lock_file, 'w')
        lock_h.write(str(datetime.datetime.now()))
        lock_h.close()
        for folder in os.listdir(settings.DOSSIERS_VALIDES_PATH):
            folder_path = os.path.join(settings.DOSSIERS_VALIDES_PATH, folder)
            # report files other than lock
            if os.path.isfile(folder_path):
                if folder != 'lock':
                    log_h.write('{} found\n'.format(folder_path))
                    log_h.flush()
            # folders
            else:
                if folder.endswith('.pb'):
                    continue
                time_since_last_mod = time.time() - os.path.getmtime(folder_path)
                if time_since_last_mod > 10:
                    log_h.write('folder {} has been unchanged for {} seconds. Will try to incorpotate ...'
                                .format(folder, time_since_last_mod))
                    log_h.flush()
                    incorporate_dossier(folder_path, False)
                    log_h.write('Done\n')
                    log_h.flush()
        # delete lock file
        os.remove(lock_file)
        log_h.close()
