import os
import logging
import shutil
from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Do basic checks in XP'

    completer_path = '/var/dav_levo/completer/'
    dossier_path = '/var/levo/dossiers/'

    def add_arguments(self, parser):
        parser.add_argument('--real', dest='dry_run', action='store_false')
        parser.set_defaults(dry_run=True)

    def handle(self, *args, **options):
        dry_run = options['dry_run']
        if not dry_run:
            logger.warning("This is for real")

        for folder in os.listdir(Command.completer_path):
            folder_path = os.path.join(Command.completer_path, folder)
            # ignore files
            if os.path.isfile(folder_path):
                logger.warning('{} found\n'.format(folder_path))
            # folders
            else:
                merge_folders(folder_path, os.path.join(Command.dossier_path, folder), dry_run)


def merge_folders(dossier_path, new_dossier_path, dry_run):
    if os.path.isdir(new_dossier_path):
        for dir_path, subdirList, fileList in os.walk(dossier_path):
            for filename in fileList:
                file_path = os.path.join(dir_path, filename)
                new_file_path = os.path.join(new_dossier_path, (os.path.relpath(file_path, dossier_path)))
                if os.path.getsize(file_path) > 0:
                    parent, dir_name = os.path.split(new_file_path)
                    if not os.path.isdir(parent):
                        logger.warning("Will create directory {}".format(parent))
                        if not dry_run:
                            os.makedirs(parent)
                    logger.warning("Will move {} to {}".format(file_path, new_file_path))
                    if os.path.isfile(new_file_path):
                        logger.warning("{} exists and will be overwritten".format(new_file_path))
                    if not dry_run:
                        shutil.move(file_path, new_file_path)
                # if the file to be copied is empty, it means the destination should be removed
                else:
                    logger.warning("File {} has size 0 ".format(file_path))
                    if os.path.isfile(new_file_path):
                        logger.warning("{} exists and will be deleted".format(new_file_path))
                        if not dry_run:
                            os.remove(file_path)
                    else:
                        logger.warning("{} doesn't exist, ignoring".format(new_file_path))
                    if not dry_run:
                        os.remove(new_file_path)

            if sum([len(files) for r, d, files in os.walk(dossier_path)]) == 0:
                logger.warning("{} is empty and will be deleted".format(dossier_path))
                if not dry_run:
                    shutil.rmtree(dossier_path)
            else:
                logger.warning("{} will not be deleted because it still contains files.\n".format(dossier_path))
    else:
        logger.warning("{} doesn't exist, will simply move {} there".format(new_dossier_path, dossier_path))
        if not dry_run:
            os.rename(dossier_path, new_dossier_path)
