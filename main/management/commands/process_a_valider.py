import os
import time
from django.core.management.base import BaseCommand
from django.conf import settings
from main.dossier import check_folder


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

# def add_arguments(self, parser):
    # parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        for folder in os.listdir(settings.DAV_DOSSIERS_A_VALIDER_PATH):
            folder_path = os.path.join(settings.DAV_DOSSIERS_A_VALIDER_PATH, folder)
            # ignore files
            if os.path.isfile(folder_path):
                print('{} should not be here'.format(folder_path))
            # folders
            else:
                time_since_last_mod = time.time() - os.path.getmtime(folder_path)
                print(time_since_last_mod)

                if time_since_last_mod > 300:
                    if check_folder(folder_path, False):
                        if not os.path.isfile(os.path.join(folder_path, str(folder) + '.6.pdf')):
                            destination_path = os.path.join(settings.DAV_DOSSIERS_AVOCATS_PATH, '6NP', folder)
                        else:
                            destination_path = os.path.join(settings.DAV_DOSSIERS_AVOCATS_PATH, folder)
                        os.rename(folder_path, destination_path)
