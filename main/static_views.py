from django.shortcuts import render


def les_avocats(request):
    return render(request, 'les_avocats.html')


def le_cabinet(request):
    return render(request, 'le_cabinet.html')


def les_avocats_partenaires(request):
    return render(request, 'les_avocats_partenaires.html')


def les_actions_collectives(request):
    return render(request, 'les_actions_collectives.html')


def la_plateforme(request):
    return render(request, 'la_plateforme.html')
