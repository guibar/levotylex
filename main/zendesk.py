import json
import requests
from levotylex.delayed_jobs import postpone
import logging
from django.template.loader import render_to_string

tag_dossier_recu = 'dossier_recu'
user_auth = 'g.barreau@gmail.com' + '/token'
token = '1I5Clwu8guVrnrKXspr2vZj3w7HNN1nLZUCqMVtO'
headers = {'content-type': 'application/json'}

url_user_search = 'https://mysmartcab.zendesk.com/api/v2/users/search.json?query={}'
url_user_create = 'https://mysmartcab.zendesk.com/api/v2/users.json'
url_ticket_create = 'https://mysmartcab.zendesk.com/api/v2/tickets.json'
url_ticket_update = 'https://mysmartcab.zendesk.com/api/v2/tickets/{}.json'
url_ticket_search = 'https://mysmartcab.zendesk.com/api/v2/search.json?{}'
url_ticket_tag_update = 'https://mysmartcab.zendesk.com/api/v2/tickets/update_many.json?ids={}'

logger = logging.getLogger(__name__)


def get_zendesk_user(user):
    # user already has the zd id set
    if user.zendesk_id:
        return user.zendesk_id
    # user doesn't have a zd id
    else:
        # look for zd user with the same email
        response = requests.get(url_user_search.format(user.email), auth=(user_auth, token), headers=headers)
        if response.status_code != 200:
            # problem with the request
            logger.error('Status: {}, Problem with ZD user search {}. Exiting.'
                         .format(response.status_code, user.email))
            return None
        resp_json = response.json()
        # there is a user with this email, update the django user
        if len(resp_json['users']) == 1:
            user.zendesk_id = resp_json['users'][0]['id']
            user.save()
            return user.zendesk_id
        # no user with this email
        else:
            new_user = {"user": {"name": user.get_full_name(), "email": user.email, "phone": user.telephone,
                                 "user_fields": {"id_django": user.id}}}
            payload = json.dumps(new_user)
            response = requests.post(url_user_create, data=payload, auth=(user_auth, token), headers=headers)
            if response.status_code != 201:
                logger.error('Status: {}, Problem with ZD user creation {}. Exiting.'
                             .format(response.status_code, user.email))
                return None
            resp_json = response.json()
            user.zendesk_id = resp_json['user']['id']
            user.save()
            return user.zendesk_id


def find_dossier_ticket(zendesk_user_id):
    # find ticket of type task, with tag dossier belonging to user
    query = "query=type:ticket ticket_type:task tags:dossier requester_id:{}"
    q = query.format(zendesk_user_id)
    response = requests.get(url_ticket_search.format(q), auth=(user_auth, token), headers=headers)
    if response.status_code != 200:
        logger.error('Status: {}, Problem with ZD dossier search for user {}. Exiting.'
                     .format(response.status_code, zendesk_user_id))
        return None
    resp_json = response.json()
    # there is a user with this email, update the django user
    if len(resp_json['results']) > 0:
        return resp_json['results'][0]['id']
    else:
        return -1


def update_dossier_ticket(ticket_id, html_body, public, tags=None):
    payload = json.dumps({'ticket': {'comment': {'public': public, 'html_body': html_body}}})
    # Do the HTTP post request
    response = requests.put(url_ticket_update.format(ticket_id), data=payload, auth=(user_auth, token), headers=headers)
    if response.status_code != 200:
        logger.error('Status: {}, Problem with ZD dossier update for ticket {}. Exiting.'
                     .format(response.status_code, ticket_id))
        return None
    if (tags):
        payload = json.dumps({'ticket': {'additional_tags': tags}})
        response = requests.put(url_ticket_tag_update.format(ticket_id), data=payload,
                                auth=(user_auth, token), headers=headers)
        if response.status_code != 200:
            logger.error('Status: {}, Problem with ZD dossier tag update for ticket {}. Exiting.'
                         .format(response.status_code, ticket_id))
            return None
    return 1


def create_dossier_ticket(zendesk_user_id, c_body):
    subject_dossier = "Votre dossier pour l'action Levothyrox"
    data1 = {'ticket': {'requester_id': zendesk_user_id, 'type': 'task', 'subject': subject_dossier, 'tags': 'dossier',
                        'comment': {'public': 'false', 'html_body': c_body}}}
    # Encode the data to create a JSON payload
    payload = json.dumps(data1)
    response = requests.post(url_ticket_create, data=payload, auth=(user_auth, token), headers=headers)
    # Check for HTTP codes other than 201 (Created)
    if response.status_code != 201:
        logger.error('Status: {}, Problem with ZD dossier creation for user {}. Exiting.'
                     .format(response.status_code, zendesk_user_id))
        return None
    return response.json()['ticket']['id']


def create_or_update_dossier_ticket(user):
    zendesk_user_id = get_zendesk_user(user)

    date = user.payment_date if user.payment_date else 'Non Renseignée'
    if user.payment_received:
        trans_id = user.payment_trans_id if user.payment_trans_id else -1
        c_body = "Date du paiement: {} . <br>Num. de transaction: {}".format(date, trans_id)
    elif user.payment_par_cheque:
        n_cheque = user.num_cheque if user.num_cheque else 'Non Renseigné'
        c_body = "Paiement par chèque<br>Date du paiement: {} <br>Num. de chèque: {}".format(date, n_cheque)
    elif user.payment_par_virement:
        c_body = "Paiement par virement<br>Date du paiement: {} .".format(date)
    # do nothing, user hasn't paid
    else:
        return

    # check if there is a dossier ticket
    ticket_id = find_dossier_ticket(zendesk_user_id)
    # no ticket found, create one
    if ticket_id < 0:
        user.dossier.zendesk_id = create_dossier_ticket(zendesk_user_id, c_body)
        user.dossier.save()
    else:
        user.dossier.zendesk_id = ticket_id
        user.dossier.save()
        update_dossier_ticket(ticket_id, c_body, 'false')


@postpone
def send_dossier_ack_zd(dossier):
    dossier_recu_html = render_to_string('dossier_recu.html')
    update_dossier_ticket(dossier.zendesk_id, dossier_recu_html, 'true', [tag_dossier_recu])


def create_post_invoice_ticket(user):
    zendesk_user_id = get_zendesk_user(user)
    subject = "Envoyer la facture de {} par la poste".format(user.get_full_name())
#    assignee_id = '114995452054'  # guillaume en test
    assignee_id = '115012527813'  # alex
    c_body = ("Envoyer la facture de l'utilisateur {} qui n'a pas d'email validé.<br> {} "
              .format(user.id, user.link_to_facture_abs()))
    data1 = {'ticket': {'requester_id': zendesk_user_id, 'assignee_id': assignee_id,
                        'subject': subject, 'tags': 'poster_facture',
                        'comment': {'public': 'false', 'html_body': c_body}}}
    # Encode the data to create a JSON payload
    payload = json.dumps(data1)
    response = requests.post(url_ticket_create, data=payload, auth=(user_auth, token), headers=headers)
    # Check for HTTP codes other than 201 (Created)
    if response.status_code != 201:
        logger.error('Status: {}, Problem with ZD dossier creation for user {}. Exiting.'
                     .format(response.status_code, zendesk_user_id))
        return None
    return response.json()['ticket']['id']


# user_gbg = User.objects.filter(email='guibargui@gmail.com').first()
# dossier_test = Dossier.objects.filter(owner_id=8575).first()
# print(dossier_test.zendesk_id)
# send_ack_zd(dossier_test)

# update_dossier_ticket(2469,'hello boy','true',['tag1','tag2'])
# dossier_recu_html = render_to_string('dossier_recu.html')
# update_dossier_ticket(2469,dossier_recu_html,'true',[tag_dossier_recu])

# print(create_or_update_dossier_ticket(user_gbg))

# print(find_dossier_ticket(115559156994))
# update_ticket_dossier(2358,'ceci est un gros test',True,['abc'] )
